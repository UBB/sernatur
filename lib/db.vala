/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * The sernatur library
 */
namespace LibSernatur {
	/**
	 * Sernatur database library
	 */
	namespace DB {
		using Postgres;

		/**
		 * The errors that can be thrown by Postgresql
		 */
		public errordomain PostgresError {
			/**
			 * Connection error
			 */
			CONNECT,
			/**
			 * Escape error
			 */
			ESCAPE
		}

		/**
		 * The errors that can be thrown while doing queries in the database
		 */
		public errordomain DBError {
			/**
			 * Invalid value
			 */
			INVALID_VALUE,
			/**
			 * Could not be modified or deleted because of a constraint
			 */
			FOREIGN_KEY_CONSTAINT
		}

		/**
		 * Class to handle database connections
		 */
		public class Connection : Object {
			/**
			 * The database connection
			 */
			public Database db;

			/**
			 * Initialize the connection
			 * @param host The hostname
			 * @param port The port
			 * @param options The extra options to use
			 * @param tty The terminal to send output to, this is ignored
			 * @param database The database
			 * @param username The username
			 * @param password The password
			 * @throws PostgresError The error thrown when connection fails
			 */
			public Connection (string host, string port, string options, string tty, string database, string username, string password) throws PostgresError {
				db = set_db_login (host, port, options, tty, database, username, password);
				if (db.get_status () != Postgres.ConnectionStatus.OK) {
					throw new PostgresError.CONNECT (db.get_error_message ());
				}
				GLib.print (_ ("Postgresql server version:") + " %d\n", db.get_server_version ());
			}

			/**
			 * Escape a string to be safe to use in Postgresql queries
			 * @param str The string to escape
			 * @return Returns the escaped string
			 * @throws PostgresError Thrown if there was some problem escaping the string
			 */
			public string escape (string str) throws PostgresError {
				string* to = malloc ((sizeof (string) * str.length * 2) + 1); // to has to be double the size of str + 1
				int error_code;
				db.escape_string_conn (to, str, str.length, out error_code);
				if (error_code != 0) {
					throw new PostgresError.ESCAPE (db.get_error_message ());
				}

				string result = to;
				free (to);
				return result;
			}
		}
	}
}
