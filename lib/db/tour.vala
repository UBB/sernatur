/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The Tour class based on the database table
		 */
		public class Tour : Object {
			/**
			 * The tour id
			 */
			public uint id_tour { get; set; default = 0; }
			/**
			 * The tour name
			 */
			public string nombre_tour { get; set; default = ""; }
			/**
			 * The individual cost
			 */
			public uint costo_indiv { get; set; default = 0; }
			/**
			 * The group cost
			 */
			public uint costo_grupal { get; set; default = 0; }
			/**
			 * The minimum number of people
			 */
			public uint minima_personas { get; set; default = 0; }
			/**
			 * The city
			 */
			public Ciudad ciudad { get; set; default = null; }

			/**
			 * Initialize the Tour class
			 * @param id_tour The tour id
			 * @param nombre_tour The tour name
			 * @param costo_indiv The individual cost
			 * @param costo_grupal The group cost
			 * @param minima_personas The minimum number of people
			 * @param ciudad The city
			 */
			public Tour (uint id_tour = 0, string nombre_tour = "", uint costo_indiv = 0, uint costo_grupal = 0, uint minima_personas = 0, Ciudad? ciudad = null) {
				this.id_tour = id_tour;
				this.nombre_tour = nombre_tour;
				this.costo_indiv = costo_indiv;
				this.costo_grupal = costo_grupal;
				this.minima_personas = minima_personas;
				this.ciudad = ciudad;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Tour
			 * @throws PostgresError If there is a problem with with escaping strings
			 */
			public static List<Tour> get_all_tours (Connection conn) throws PostgresError {
				var res = conn.db.exec (Query.get_query (conn, Query.Type.SELECT_ALL_TOURS));
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Tour> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Tour> list = new List<Tour> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var tour = new Tour (wra.get_int_n (i, "id_tour"),
							wra.get_string_n (i, "nombre_tour"),
							wra.get_int_n (i, "costo_indiv"),
							wra.get_int_n (i, "costo_grupal"),
							wra.get_int_n (i, "minima_personas"),
							new Ciudad (wra.get_int_n (i, "id_ciudad"),
								wra.get_string_n (i, "nombre_ciudad"),
								new Region (wra.get_int_n (i, "id_region"),
									wra.get_string_n (i, "nombre_region")
								)
							)
						);
						list.append (tour);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}

			/**
			 * Update a tour in the database
			 * @param conn The database connection
			 * @param tour The tour to update
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static void update_tour (Connection conn, Tour tour) throws PostgresError, DBError {
				if (tour.id_tour == 0) {
					throw new DBError.INVALID_VALUE (_ ("The id of the tour is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.UPDATE_TOUR, tour));
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}

			/**
			 * Insert a tour in the database
			 * @param conn The database connection
			 * @param tour The tour to insert
			 * @return Returns the id of the tuple inserted
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static uint insert_tour (Connection conn, Tour tour) throws PostgresError, DBError {
				if (tour.id_tour != 0) {
					throw new DBError.INVALID_VALUE (_ ("The id of the tour is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.INSERT_TOUR, tour));
				// This uses TUPLES_OK because it returns a result which is the id of the inserted tour
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}

				var wra = new ResultWrapper (res);
				try {
					return wra.get_int_n (0, "id_tour");
				}
				catch (Error e) {
					#if DEBUG
						error (e.message);
					#else
						warning (e.message);
						return 0;
					#endif
				}
			}

			/**
			 * Delete a tour in the database
			 * @param conn The database connection
			 * @param tour The tour to delete
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static void delete_tour (Connection conn, Tour tour) throws PostgresError, DBError {
				if (tour.id_tour == 0) {
					throw new DBError.INVALID_VALUE (_ ("The id of the tour is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.DELETE_TOUR, tour));
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					if (res.get_error_field (FieldCode.SQLSTATE) == "23503") {
						throw new DBError.FOREIGN_KEY_CONSTAINT (res.get_error_field (FieldCode.MESSAGE_PRIMARY));
					}
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}
		}
	}
}
