/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The Turista class based on the database table
		 */
		public class Turista : Object {
			/**
			 * The RUT of the touristist
			 */
			public string rut_turista { get; set; default = ""; }
			/**
			 * The touristist name
			 */
			public string nombre_turista { get; set; default = ""; }
			/**
			 * The birth date of the touristist
			 */
			public string fecha_nacimento { get; set; default = ""; }
			/**
			 * The emergency contact
			 */
			public ContactoEmergencia contacto_emergencia { get; set; default = null; }

			/**
			 * Initilize the Turista class
			 * @param rut_turista The RUT of the touristist
			 * @param nombre_turista The touristist name
			 * @param fecha_nacimento The birth date of the touristist
			 * @param contacto_emergencia The emergency contact
			 */
			public Turista (string rut_turista = "", string nombre_turista = "", string fecha_nacimento = "", ContactoEmergencia? contacto_emergencia = null) {
				this.rut_turista = rut_turista;
				this.nombre_turista = nombre_turista;
				this.fecha_nacimento = fecha_nacimento;
				this.contacto_emergencia = contacto_emergencia;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Turista
			 * @throws PostgresError If there is a problem with with escaping strings
			 */
			public static List<Turista> get_all_turistas (Connection conn) throws PostgresError {
				var res = conn.db.exec (Query.get_query (conn, Query.Type.SELECT_ALL_TOURISTS));
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Turista> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Turista> list = new List<Turista> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var turista = new Turista (wra.get_string_n (i, "rut_turista"),
							wra.get_string_n (i, "nombre_turista"),
							wra.get_string_n (i, "fecha_nacimento"),
							new ContactoEmergencia (wra.get_int_n (i, "id_contacto"),
								wra.get_int_n (i, "telefono_emergencia"),
								wra.get_string_n (i, "nombre_emergencia")
							)
						);
						list.append (turista);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}

			/**
			 * Get a single tuple and field from database
			 * @param conn The database connection to use
			 * @return Returns a Turista
			 * @throws PostgresError If there is a problem with with escaping strings
			 */
			public static Turista? get_turista_by_run (Connection conn, string run) throws PostgresError {
				var res = conn.db.exec (Query.get_query (conn, Query.Type.SELECT_TOURIST_BY_RUN, run));
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return null;
					#endif
				}

				var wra = new ResultWrapper (res);
				Turista turista = null;
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						turista = new Turista (wra.get_string_n (i, "rut_turista"),
							wra.get_string_n (i, "nombre_turista"),
							wra.get_string_n (i, "fecha_nacimento"),
							new ContactoEmergencia (wra.get_int_n (i, "id_contacto"),
								wra.get_int_n (i, "telefono_emergencia"),
								wra.get_string_n (i, "nombre_emergencia")
							)
						);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return turista;
			}

			/**
			 * Update a tourist in the database
			 * @param conn The database connection
			 * @param tourist The tourist to update
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static void update_tourist (Connection conn, Turista tourist, string original_run) throws PostgresError, DBError {
				if (tourist.rut_turista.strip () == "") {
					throw new DBError.INVALID_VALUE (_ ("The RUN of the tourist is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.UPDATE_TOURIST, tourist, original_run));
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}

			/**
			 * Insert a tourist in the database
			 * @param conn The database connection
			 * @param tourist The tourist to insert
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static void insert_tourist (Connection conn, Turista tourist) throws PostgresError, DBError {
				if (tourist.rut_turista.strip () == "") {
					throw new DBError.INVALID_VALUE (_ ("The RUN of the tourist is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.INSERT_TOURIST, tourist));
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}

			/**
			 * Delete a tourist in the database
			 * @param conn The database connection
			 * @param tourist The tourist to delete
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static void delete_tourist (Connection conn, Turista tourist) throws PostgresError, DBError {
				if (tourist.rut_turista.strip () == "") {
					throw new DBError.INVALID_VALUE (_ ("The RUN of the tourist is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.DELETE_TOURIST, tourist));
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					if (res.get_error_field (FieldCode.SQLSTATE) == "23503") {
						throw new DBError.FOREIGN_KEY_CONSTAINT (res.get_error_field (FieldCode.MESSAGE_PRIMARY));
					}
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}
		}
	}
}
