/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The Realiza class based on the database table
		 */
		public class Realiza : Object {
			/**
			 * The tour
			 */
			public Tour tour { get; set; default = null; }
			/**
			 * The tourist
			 */
			public Turista turista { get; set; default = null; }
			/**
			 * The discount
			 */
			public Descuento descuento { get; set; default = null; }

			/**
			 * Initialize the Realiza class
			 * @param tour The tour
			 * @param turista The tourist
			 * @param descuento The discount
			 */
			public Realiza (Tour? tour = null, Turista? turista = null, Descuento? descuento = null) {
				this.tour = tour;
				this.turista = turista;
				this.descuento = descuento;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Realiza
			 */
			public static List<Realiza> get_all_realizas (Connection conn) {
				var res = conn.db.exec ("
SELECT T.id_tour, T.nombre_tour, T.costo_indiv, T.costo_grupal, T.minima_personas,
C.id_ciudad, C.nombre_ciudad,
R.id_region, R.nombre_region,
T2.rut_turista, T2.nombre_turista, T2.fecha_nacimento,
C2.id_contacto, C2.telefono_emergencia, C2.nombre_emergencia,
D.id_descuento, D.descripcion_descuento, D.porcentaje
FROM realiza RE
JOIN tour T ON (RE.id_tour = T.id_tour)
JOIN ciudad C ON (T.id_ciudad = C.id_ciudad)
JOIN region R ON (C.id_region = R.id_region)
JOIN turista T2 ON (RE.rut_turista = T2.rut_turista)
JOIN contacto_emergencia C2 ON (T2.id_contacto = C2.id_contacto)
JOIN descuento D ON (RE.id_descuento = D.id_descuento)
				");
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Realiza> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Realiza> list = new List<Realiza> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var realiza = new Realiza (
							new Tour (wra.get_int_n (i, "id_tour"),
								wra.get_string_n (i, "nombre_tour"),
								wra.get_int_n (i, "costo_indiv"),
								wra.get_int_n (i, "costo_grupal"),
								wra.get_int_n (i, "minima_personas"),
								new Ciudad (wra.get_int_n (i, "id_ciudad"),
									wra.get_string_n (i, "nombre_ciudad"),
									new Region (wra.get_int_n (i, "id_region"),
										wra.get_string_n (i, "nombre_region")
									)
								)
							),
							new Turista (wra.get_string_n (i, "rut_turista"),
								wra.get_string_n (i, "nombre_turista"),
								wra.get_string_n (i, "fecha_nacimento"),
								new ContactoEmergencia (wra.get_int_n (i, "id_contacto"),
									wra.get_int_n (i, "telefono_emergencia"),
									wra.get_string_n (i, "nombre_emergencia")
								)
							),
							new Descuento (wra.get_int_n (i, "id_descuento"),
								wra.get_string_n (i, "descripcion_descuento"),
								wra.get_float_n (i, "porcentaje")
							)
						);
						list.append (realiza);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}
		}
	}
}
