/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The Region class based on the database table
		 */
		public class Region : Object {
			/**
			 * The id of the region
			 */
			public uint id_region { get; set; default = 0; }
			/**
			 * The name of the region
			 */
			public string nombre_region { get; set; default = ""; }

			/**
			 * Initialize the Region class
			 * @param id_region The region id
			 * @param nombre_region The region name
			 */
			public Region (uint id_region = 0, string nombre_region = "") {
				this.id_region = id_region;
				this.nombre_region = nombre_region;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Region
			 * @throws PostgresError If there is a problem with with escaping strings
			 */
			public static List<Region> get_all_regiones (Connection conn) throws PostgresError {
				var res = conn.db.exec (Query.get_query (conn, Query.Type.SELECT_ALL_REGIONS, null));
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Region> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Region> list = new List<Region> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var region = new Region (wra.get_int_n (i, "id_region"),
							wra.get_string_n (i, "nombre_region")
						);
						list.append (region);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}

			/**
			 * Insert a region in the database
			 * @param conn The database connection
			 * @param region The region to insert
			 * @return Returns the id of the tuple inserted
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static uint insert_region (Connection conn, Region region) throws PostgresError, DBError {
				if (region.id_region != 0) {
					throw new DBError.INVALID_VALUE (_ ("The id of the region is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.INSERT_REGION, region));
				// This uses TUPLES_OK because it returns a result which is the id of the inserted region
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}

				var wra = new ResultWrapper (res);
				try {
					return wra.get_int_n (0, "id_region");
				}
				catch (Error e) {
					#if DEBUG
						error (e.message);
					#else
						warning (e.message);
						return 0;
					#endif
				}
			}
		}
	}
}
