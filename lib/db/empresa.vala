/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The Empresa class based on the database table
		 */
		public class Empresa : Object {
			/**
			 * The RUT of the company
			 */
			public string rut_empresa { get; set; default = ""; }
			/**
			 * The name of the company
			 */
			public string nombre_empresa { get; set; default = ""; }
			/**
			 * The name of the contact in the company
			 */
			public string contacto { get; set; default = ""; }
			/**
			 * The phone number of the contact
			 */
			public uint telefono { get; set; default = 0; }

			/**
			 * Initialize the Empresa class
			 * @param rut_empresa The RUT of the company
			 * @param nombre_empresa The name of the company
			 * @param contacto The name of the contact in the company
			 * @param telefono The phone number of the contact
			 */
			public Empresa (string rut_empresa = "", string nombre_empresa = "", string contacto = "", uint telefono = 0) {
				this.rut_empresa = rut_empresa;
				this.nombre_empresa = nombre_empresa;
				this.contacto = contacto;
				this.telefono = telefono;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Empresa
			 */
			public static List<Empresa> get_all_empresas (Connection conn) {
				var res = conn.db.exec ("
SELECT rut_empresa, nombre_empresa, contacto, telefono FROM empresa
				");
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Empresa> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Empresa> list = new List<Empresa> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var empresa = new Empresa (wra.get_string_n (i, "rut_empresa"),
							wra.get_string_n (i, "nombre_empresa"),
							wra.get_string_n (i, "contacto"),
							wra.get_int_n (i, "telefono")
						);
						list.append (empresa);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}
		}
	}
}
