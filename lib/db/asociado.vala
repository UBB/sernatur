/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The Asociado class based on the database table
		 */
		public class Asociado : Object {
			public string fecha_llegada { get; set; default = ""; }
			public string hora_llegada { get; set; default = ""; }
			public string fecha_salida { get; set; default = ""; }
			public string hora_salida { get; set; default = ""; }
			public Tour tour { get; set; default = null; }
			public Lugar lugar { get; set; default = null; }

			/**
			 * Initialize the Asociado class
			 * @param fecha_llegada The date of arrival
			 * @param hora_llegada The time of arrival
			 * @param fecha_salida The date of departure
			 * @param hora_salida The time of departure
			 * @param tour The tour associated
			 * @param lugar The place associated
			 */
			public Asociado (string fecha_llegada = "", string hora_llegada = "", string fecha_salida = "", string hora_salida = "", Tour? tour = null, Lugar? lugar = null) {
				this.fecha_llegada = fecha_llegada;
				this.hora_llegada = hora_llegada;
				this.fecha_salida = fecha_salida;
				this.hora_salida = hora_salida;
				this.tour = tour;
				this.lugar = lugar;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Asociado
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 */
			public static List<Asociado> get_all_asociados (Connection conn) throws PostgresError {
				var res = conn.db.exec (Query.get_query (conn, Query.Type.SELECT_ALL_ASSOCIATED, null));
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Asociado> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Asociado> list = new List<Asociado> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var asociado = new Asociado (wra.get_string_n (i, "fecha_llegada"),
							wra.get_string_n (i, "hora_llegada"),
							wra.get_string_n (i, "fecha_salida"),
							wra.get_string_n (i, "hora_salida"),
							new Tour (wra.get_int_n (i, "id_tour"),
								wra.get_string_n (i, "nombre_tour"),
								wra.get_int_n (i, "costo_indiv"),
								wra.get_int_n (i, "costo_grupal"),
								wra.get_int_n (i, "minima_personas"),
								new Ciudad (wra.get_int_n (i, "id_ciudad"),
									wra.get_string_n (i, "nombre_ciudad"),
									new Region (wra.get_int_n (i, "id_region"),
										wra.get_string_n (i, "nombre_region")
									)
								)
							),
							new Lugar (wra.get_int_n (i, "id_lugar"),
								wra.get_string_n (i, "nombre_lugar"),
								wra.get_int_n (i, "valor_entrada"),
								wra.get_int_n (i, "nivel"),
								new Ciudad (wra.get_int_n (i, "id_ciudad_lugar"),
									wra.get_string_n (i, "nombre_ciudad_lugar"),
									new Region (wra.get_int_n (i, "id_region_lugar"),
										wra.get_string_n (i, "nombre_region_lugar")
									)
								)
							)
						);
						list.append (asociado);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Asociado based off a tour id
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static List<Asociado> get_all_asociados_by_tour (Connection conn, Tour tour) throws PostgresError, DBError {
				if (tour.id_tour == 0) {
					throw new DBError.INVALID_VALUE (_ ("The id of the tour is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.SELECT_ALL_ASSOCIATED_BY_TOUR, tour));
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Asociado> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Asociado> list = new List<Asociado> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var asociado = new Asociado (wra.get_string_n (i, "fecha_llegada"),
							wra.get_string_n (i, "hora_llegada"),
							wra.get_string_n (i, "fecha_salida"),
							wra.get_string_n (i, "hora_salida"),
							new Tour (wra.get_int_n (i, "id_tour"),
								wra.get_string_n (i, "nombre_tour"),
								wra.get_int_n (i, "costo_indiv"),
								wra.get_int_n (i, "costo_grupal"),
								wra.get_int_n (i, "minima_personas"),
								new Ciudad (wra.get_int_n (i, "id_ciudad"),
									wra.get_string_n (i, "nombre_ciudad"),
									new Region (wra.get_int_n (i, "id_region"),
										wra.get_string_n (i, "nombre_region")
									)
								)
							),
							new Lugar (wra.get_int_n (i, "id_lugar"),
								wra.get_string_n (i, "nombre_lugar"),
								wra.get_int_n (i, "valor_entrada"),
								wra.get_int_n (i, "nivel"),
								new Ciudad (wra.get_int_n (i, "id_ciudad_lugar"),
									wra.get_string_n (i, "nombre_ciudad_lugar"),
									new Region (wra.get_int_n (i, "id_region_lugar"),
										wra.get_string_n (i, "nombre_region_lugar")
									)
								)
							)
						);
						list.append (asociado);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}

			/**
			 * Insert an association in the database
			 * @param conn The database connection
			 * @param asociado The association to insert
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static void insert_asociado (Connection conn, Asociado asociado) throws PostgresError, DBError {
				if (asociado.tour.id_tour == 0 || asociado.lugar.id_lugar == 0) {
					throw new DBError.INVALID_VALUE (_ ("The id of the tour or the place is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.INSERT_ASSOCIATED, asociado));
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}

			/**
			 * Delete an association in the database
			 * @param conn The database connection
			 * @param asociado The association to delete
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static void delete_associated (Connection conn, Asociado asociado) throws PostgresError, DBError {
				if (asociado.tour.id_tour == 0 || asociado.lugar.id_lugar == 0) {
					throw new DBError.INVALID_VALUE (_ ("The id of the tour or place is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.DELETE_TOUR, asociado));
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					if (res.get_error_field (FieldCode.SQLSTATE) == "23503") {
						throw new DBError.FOREIGN_KEY_CONSTAINT (res.get_error_field (FieldCode.MESSAGE_PRIMARY));
					}
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}
		}
	}
}
