/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The Arrienda class based on the database table
		 */
		public class Arrienda : Object {
			/**
			 * The price
			 */
			public uint precio { get; set; default = 0; }
			/**
			 * The date the vehicle needs to be returned
			 */
			public string fecha_devolucion { get; set; default = ""; }
			/**
			 * The vehicle
			 */
			public Vehiculo vehiculo { get; set; default = null; }
			/**
			 * The compnay
			 */
			public Empresa empresa { get; set; default = null; }

			/**
			 * Initialize the Arrienda class
			 * @param precio The price
			 * @param fecha_devolucion The date when the vehicle has to be returned
			 * @param vehiculo The Vehicle
			 * @param empresa The company
			 */
			public Arrienda (uint precio = 0, string fecha_devolucion = "", Vehiculo? vehiculo = null, Empresa? empresa = null) {
				this.precio = precio;
				this.fecha_devolucion = fecha_devolucion;
				this.vehiculo = vehiculo;
				this.empresa = empresa;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Arrienda
			 */
			public static List<Arrienda> get_all_arriendas (Connection conn) {
				var res = conn.db.exec ("
SELECT A.precio, A.fecha_devolucion,
V.patente, V.ano_vehiculo, V.marca, V.capacidad,
E.rut_empresa, E.nombre_empresa, E.contacto, E.telefono
FROM arrienda A
JOIN vehiculo V ON (A.patente = V.patente)
JOIN empresa E ON (A.rut_empresa = E.rut_empresa)
				");
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Arrienda> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Arrienda> list = new List<Arrienda> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var arrienda = new Arrienda (wra.get_int_n (i, "precio"),
							wra.get_string_n (i, "fecha_devolucion"),
							new Vehiculo (wra.get_string_n (i, "patente"),
								wra.get_int_n (i, "ano_vehiculo"),
								wra.get_string_n (i, "marca"),
								wra.get_int_n (i, "capacidad")
							),
							new Empresa (wra.get_string_n (i, "rut_empresa"),
								wra.get_string_n (i, "nombre_empresa"),
								wra.get_string_n (i, "contacto"),
								wra.get_int_n (i, "telefono")
							)
						);
						list.append (arrienda);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}
		}
	}
}
