/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The Posee class based on the database table
		 */
		public class Posee : Object {
			/**
			 * The specialty level
			 */
			public uint nivel_especialidad { get; set; default = 0; }
			/**
			 * The guide
			 */
			public Guia guia { get; set; default = null; }
			/**
			 * The speciality
			 */
			public Especialidad especialidad { get; set; default = null; }

			/**
			 * Initialize the Posee class
			 * @param nivel_especialidad The specialty level
			 * @param guia The guide
			 * @param especialidad The specialty
			 */
			public Posee (uint nivel_especialidad = 0, Guia? guia = null, Especialidad? especialidad = null) {
				this.nivel_especialidad = nivel_especialidad;
				this.guia = guia;
				this.especialidad = especialidad;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Posee
			 */
			public static List<Posee> get_all_posees (Connection conn) {
				var res = conn.db.exec ("
SELECT P.nivel_especialidad,
G.rut_guia, G.nombre_guia, G.calle, G.numero,
C.id_ciudad, C.nombre_ciudad,
R.id_region, R.nombre_region,
E.id_especialidad, E.descripcion_especialidad
FROM posee P
JOIN guia G on (P.rut_guia = G.rut_guia)
JOIN ciudad C ON (G.id_ciudad = C.id_ciudad)
JOIN region R ON (C.id_region = R.id_region)
JOIN especialidad E ON (P.id_especialidad = E.id_especialidad)
				");
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Posee> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Posee> list = new List<Posee> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var posee = new Posee (wra.get_int_n (i, "nivel_especialidad"),
							new Guia (wra.get_string_n (i, "rut_guia"),
								wra.get_string_n (i, "nombre_guia"),
								wra.get_string_n (i, "calle"),
								wra.get_int_n (i, "numero"),
								new Ciudad (wra.get_int_n (i, "id_ciudad"),
									wra.get_string_n (i, "nombre_ciudad"),
									new Region (wra.get_int_n (i, "id_region"),
										wra.get_string_n (i, "nombre_region")
									)
								)
							),
							new Especialidad (wra.get_int_n (i, "id_especialidad"),
								wra.get_string_n (i, "descripcion_especialidad")
							)
						);
						list.append (posee);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}
		}
	}
}
