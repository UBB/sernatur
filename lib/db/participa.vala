/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The Participa class based on the database table
		 */
		public class Participa : Object {
			/**
			 * The tour
			 */
			public Tour tour { get; set; default = null; }
			/**
			 * The guide
			 */
			public Guia guia { get; set; default = null; }
			/**
			 * The category
			 */
			public Categoria categoria { get; set; default = null; }

			/**
			 * Initialize the Participa class
			 * @param tour The tour
			 * @param guia The guide
			 * @param categoria The category
			 */
			public Participa (Tour? tour = null, Guia? guia = null, Categoria? categoria = null) {
				this.tour = tour;
				this.guia = guia;
				this.categoria = categoria;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Participa
			 */
			public static List<Participa> get_all_participas (Connection conn) {
				var res = conn.db.exec ("
SELECT T.id_tour, T.nombre_tour, T.costo_indiv, T.costo_grupal, T.minima_personas,
C.id_ciudad, C.nombre_ciudad,
R.id_region, R.nombre_region,
G.rut_guia, G.nombre_guia, G.calle, G.numero,
C2.id_ciudad AS id_ciudad_guia, C2.nombre_ciudad AS nombre_ciudad_guia,
R2.id_region AS id_region_guia, R2.nombre_region AS nombre_region_guia,
C3.id_categoria, C3.descripcion_categoria
FROM participa P
JOIN tour T ON (P.id_tour = T.id_tour)
JOIN ciudad C ON (T.id_ciudad = C.id_ciudad)
JOIN region R on (C.id_region = R.id_region)
JOIN guia G on (P.rut_guia = G.rut_guia)
JOIN ciudad C2 ON (G.id_ciudad = C2.id_ciudad)
JOIN region R2 ON (C2.id_region = R2.id_region)
JOIN categoria C3 ON (P.id_categoria = C3.id_categoria)
				");
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Participa> ();
					#endif
				}

				var wra = new ResultWrapper(res);
				List<Participa> list = new List<Participa> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var participa = new Participa (
							new Tour (wra.get_int_n (i, "id_tour"),
								wra.get_string_n (i, "nombre_tour"),
								wra.get_int_n (i, "costo_indiv"),
								wra.get_int_n (i, "costo_grupal"),
								wra.get_int_n (i, "minima_personas"),
								new Ciudad (wra.get_int_n (i, "id_ciudad"),
									wra.get_string_n (i, "nombre_ciudad"),
									new Region (wra.get_int_n (i, "id_region"),
										wra.get_string_n (i, "nombre_region")
									)
								)
							),
							new Guia (wra.get_string_n (i, "rut_guia"),
								wra.get_string_n (i, "nombre_guia"),
								wra.get_string_n (i, "calle"),
								wra.get_int_n (i, "numero"),
								new Ciudad (wra.get_int_n (i, "id_ciudad_guia"),
									wra.get_string_n (i, "id_region_guia"),
									new Region (wra.get_int_n (i, "id_region_guia"),
										wra.get_string_n (i, "nombre_region_guia"))
									)
								),
							new Categoria (wra.get_int_n (i, "id_categoria"),
								wra.get_string_n (i, "descripcion_categoria")
							)
						);
						list.append (participa);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}
		}
	}
}
