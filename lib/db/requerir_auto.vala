/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The RequerirAuto class based on the database table
		 */
		public class RequerirAuto : Object {
			/**
			 * The chofer
			 */
			public string chofer { get; set; default = ""; }
			/**
			 * The tour
			 */
			public Tour tour { get; set; default = null; }
			/**
			 * The vehicle
			 */
			public Vehiculo vehiculo { get; set; default = null; }

			/**
			 * Initialize the RequerirAuto class
			 * @param chofer The chofer
			 * @param tour The tour
			 * @param vehiculo The vehicle
			 */
			public RequerirAuto (string chofer = "", Tour? tour = null, Vehiculo? vehiculo = null) {
				this.chofer = chofer;
				this.tour = tour;
				this.vehiculo = vehiculo;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of RequerirAuto
			 * @throws PostgresError If there is a problem with with escaping strings
			 */
			public static List<RequerirAuto> get_all_requerir_autos (Connection conn) throws PostgresError {
				var res = conn.db.exec (Query.get_query (conn, Query.Type.SELECT_ALL_VEHICLE_REQUIRED));
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<RequerirAuto> ();
					#endif
				}

				var wra = new ResultWrapper(res);
				List<RequerirAuto> list = new List<RequerirAuto> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var requerir_auto = new RequerirAuto (wra.get_string_n (i, "chofer"),
							new Tour (wra.get_int_n (i, "id_tour"),
								wra.get_string_n (i, "nombre_tour"),
								wra.get_int_n (i, "costo_indiv"),
								wra.get_int_n (i, "costo_grupal"),
								wra.get_int_n (i, "minima_personas"),
								new Ciudad (wra.get_int_n (i, "id_ciudad"),
									wra.get_string_n (i, "nombre_ciudad"),
									new Region (wra.get_int_n (i, "id_region"),
										wra.get_string_n (i, "nombre_region")
									)
								)
							),
							new Vehiculo (wra.get_string_n (i, "patente"),
								wra.get_int_n (i, "ano_vehiculo"),
								wra.get_string_n (i, "marca"),
								wra.get_int_n (i, "capacidad")
							)
						);
						list.append (requerir_auto);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}

			/**
			 * Get 1 tuple from the database based on tour id
			 * @param conn The database connection to use
			 * @return Returns a RequerirAuto
			 * @throws PostgresError If there is a problem with with escaping strings
			 */
			public static RequerirAuto? get_requerir_auto_by_tour (Connection conn, Tour tour) throws PostgresError {
				var res = conn.db.exec (Query.get_query (conn, Query.Type.SELECT_VEHICLE_REQUIRED_BY_TOUR, tour));
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return null;
					#endif
				}

				RequerirAuto requerir_auto = null;
				var wra = new ResultWrapper(res);
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						requerir_auto = new RequerirAuto (wra.get_string_n (i, "chofer"),
							tour,
							new Vehiculo (wra.get_string_n (i, "patente"),
								wra.get_int_n (i, "ano_vehiculo"),
								wra.get_string_n (i, "marca"),
								wra.get_int_n (i, "capacidad")
							)
						);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return requerir_auto;
			}

			/**
			 * Insert a require auto object in the database
			 * @param conn The database connection
			 * @param requerir_auto The require auto to insert
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static void insert_require_vehicle (Connection conn, RequerirAuto requerir_auto) throws PostgresError, DBError {
				if (requerir_auto.tour.id_tour == 0 || requerir_auto.vehiculo.patente.strip () == "") {
					throw new DBError.INVALID_VALUE (_ ("The id of the tour or the license plate for the vehicle is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.INSERT_VEHICLE_REQUIRED, requerir_auto));
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}

			/**
			 * Update a require auto object in the database
			 * @param conn The database connection
			 * @param requerir_auto The require vehicle to update
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static void update_require_vehicle (Connection conn, RequerirAuto requerir_auto) throws PostgresError, DBError {
				if (requerir_auto.tour.id_tour == 0 || requerir_auto.vehiculo.patente.strip () == "") {
					throw new DBError.INVALID_VALUE (_ ("The id of the tour or the license plate for the vehicle is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.UPDATE_VEHICLE_REQUIRED, requerir_auto));
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}

			/**
			 * Delete a required vehicle in the database
			 * @param conn The database connection
			 * @param tour The assigned tour used to delete
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static void delete_require_vehicle (Connection conn, Tour tour) throws PostgresError, DBError {
				if (tour.id_tour == 0) {
					throw new DBError.INVALID_VALUE (_ ("The id of the tour is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.DELETE_VEHICLE_REQUIRED, tour));
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					if (res.get_error_field (FieldCode.SQLSTATE) == "23503") {
						throw new DBError.FOREIGN_KEY_CONSTAINT (res.get_error_field (FieldCode.MESSAGE_PRIMARY));
					}
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}
		}
	}
}
