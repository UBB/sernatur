/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The Vehiculo class based on the database table
		 */
		public class Vehiculo : Object {
			/**
			 * The license plate
			 */
			public string patente { get; set; default = ""; }
			/**
			 * The year of the vehicle
			 */
			public uint ano_vehiculo { get; set; default = 0; }
			/**
			 * The make of the vehicle
			 */
			public string marca { get; set; default = ""; }
			/**
			 * The capacity of the vehicle
			 */
			public uint capacidad { get; set; default = 0; }

			/**
			 * Initialize the Vehiculo class
			 * @param patente The license plate
			 * @param ano_vehiculo The year of the vehicle
			 * @param marca The make of the vehicle
			 * @param capacidad The capacity of the vehicle
			 */
			public Vehiculo (string patente = "", uint ano_vehiculo = 0, string marca = "", uint capacidad = 0) {
				this.patente = patente;
				this.ano_vehiculo = ano_vehiculo;
				this.marca = marca;
				this.capacidad = capacidad;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Vehiculo
			 */
			public static List<Vehiculo> get_all_vehiculos (Connection conn) {
				var res = conn.db.exec ("
SELECT patente, ano_vehiculo, marca, capacidad FROM vehiculo
				");
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Vehiculo> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Vehiculo> list = new List<Vehiculo> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var vehiculo = new Vehiculo (wra.get_string_n (i, "patente"),
							wra.get_int_n (i, "ano_vehiculo"),
							wra.get_string_n (i, "marca"),
							wra.get_int_n (i, "capacidad")
						);
						list.append (vehiculo);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}
		}
	}
}
