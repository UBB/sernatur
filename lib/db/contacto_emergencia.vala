/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The ContactoEmergencia class based on the database table
		 */
		public class ContactoEmergencia : Object {
			/**
			 * The id of the contact
			 */
			public uint id_contacto { get; set; default = 0; }
			/**
			 * The contact's phone number
			 */
			public uint telefono_emergencia { get; set; default = 0; }
			/**
			 * The contact's name
			 */
			public string nombre_emergencia { get; set; default = ""; }

			/**
			 * Initialize the ContactoEmergencia class
			 * @param id_contacto The id of the contact
			 * @param telefono_emergencia The contact's phone number
			 * @param nombre_emergencia The contact's name
			 */
			public ContactoEmergencia (uint id_contacto = 0, uint telefono_emergencia = 0, string nombre_emergencia = "") {
				this.id_contacto = id_contacto;
				this.telefono_emergencia = telefono_emergencia;
				this.nombre_emergencia = nombre_emergencia;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of ContactoEmergencia
			 */
			public static List<ContactoEmergencia> get_all_contactos (Connection conn) {
				var res = conn.db.exec ("SELECT id_contacto, telefono_emergencia, nombre_emergencia FROM contacto_emergencia");
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<ContactoEmergencia> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<ContactoEmergencia> list = new List<ContactoEmergencia> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var contacto = new ContactoEmergencia (wra.get_int_n (i, "id_contacto"),
							wra.get_int_n (i, "telefono_emergencia"),
							wra.get_string_n (i, "nombre_emergencia")
						);
						list.append (contacto);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}
		}
	}
}
