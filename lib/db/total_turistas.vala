/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The TotalTuristas class based on the database table
		 */
		public class TotalTuristas : Object {
			/**
			 * The id of the tour
			 */
			public uint id_tour { get; set; default = 0; }
			/**
			 * The name of the tour
			 */
			public string nombre_tour { get; set; default = ""; }
			/**
			 * The total number of tourists
			 */
			public uint total_turistas { get; set; default = 0; }

			/**
			 * Initialize the TotalTuristas class
			 * @param id_tour The tour id
			 * @param nombre_tour The tour name
			 * @param total_turistas The number of tourists
			 */
			public TotalTuristas (uint id_tour = 0, string nombre_tour = "", uint total_turistas = 0) {
				this.id_tour = id_tour;
				this.nombre_tour = nombre_tour;
				this.total_turistas = total_turistas;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of TotalTuristas
			 */
			public static List<TotalTuristas> get_all_tourist_totals (Connection conn) {
				var res = conn.db.exec ("
SELECT idT, nombreT, TotalTuristas FROM TOTAL_TURISTAS
				");
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<TotalTuristas> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<TotalTuristas> list = new List<TotalTuristas> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var total = new TotalTuristas (wra.get_int_n (i, "idT"),
							wra.get_string_n (i, "nombreT"),
							wra.get_int_n (i, "TotalTuristas")
						);
						list.append (total);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}
		}
	}
}
