/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		/**
		 * The views namespace for the 5 views
		 */
		namespace Views {
			using Postgres;
			using Wrapper;

			/**
			 * The Q1 class based on the database table
			 */
			public class Q1 : Object {
				/**
				 * Get all tuples and fields from database
				 * @param conn The database connection to use
				 * @return Returns a list of RegionesSinDescuento
				 */
				public static List<RegionesSinDescuento> get_regions_without_discount (Connection conn) {
					var res = conn.db.exec ("
SELECT nombreRegion, cantidad FROM REGIONES_SINDESCUENTO WHERE (cantidad = (SELECT MAX(cantidad) FROM REGIONES_SINDESCUENTO))
					");
					if (res.get_status () != ExecStatus.TUPLES_OK) {
						#if DEBUG
							error (conn.db.get_error_message ());
						#else
							warning (conn.db.get_error_message ());
							return new List<RegionesSinDescuento> ();
						#endif
					}

					var wra = new ResultWrapper (res);
					List<RegionesSinDescuento> list = new List<RegionesSinDescuento> ();
					int n = res.get_n_tuples ();
					for (int i = 0; i < n; i++) {
						try {
							var region_sin_descuento = new RegionesSinDescuento (wra.get_string_n (i, "nombreRegion"),
								wra.get_int_n (i, "cantidad")
							);
							list.append (region_sin_descuento);
						}
						catch (Error e) {
							#if DEBUG
								error (e.message);
							#else
								warning (e.message);
							#endif
						}
					}
					return list;
				}
			}

			/**
			 * The Q2 class based on the database table
			 */
			public class Q2 : Object {
				/**
				 * The name of the tour
				 */
				public string nombre_tour { get; set; default = ""; }
				/**
				 * The value received
				 */
				public uint valor_recibido { get; set; default = 0; }

				/**
				 * Initialize the Q2 class
				 * @param nombre_tour The name of the tour
				 * @param valor_recibido The money value received
				 */
				public Q2 (string nombre_tour = "", uint valor_recibido = 0) {
					this.nombre_tour = nombre_tour;
					this.valor_recibido = valor_recibido;
				}

				/**
				 * Get all tuples and fields from database
				 * @param conn The database connection to use
				 * @return Returns a list of Q2
				 */
				public static List<Q2> get_value_received (Connection conn) {
					var res = conn.db.exec ("
SELECT V.nombreT, (V.TotalVentas - COALESCE(MAX(T.TotalDescuentos), 0)) AS ValorTotalRecibido
FROM VALORES_TOURS V FULL JOIN TOUR_DESCUENTOS T ON (T.idT = V.idT) GROUP BY (V.nombreT, V.TotalVentas)
					");
					if (res.get_status () != ExecStatus.TUPLES_OK) {
						#if DEBUG
							error (conn.db.get_error_message ());
						#else
							warning (conn.db.get_error_message ());
							return new List<Q2> ();
						#endif
					}

					var wra = new ResultWrapper (res);
					List<Q2> list = new List<Q2> ();
					int n = res.get_n_tuples ();
					for (int i = 0; i < n; i++) {
						try {
							var values = new Q2 (wra.get_string_n (i, "nombreT"),
								wra.get_int_n (i, "ValorTotalRecibido")
							);
							list.append (values);
						}
						catch (Error e) {
							#if DEBUG
								error (e.message);
							#else
								warning (e.message);
							#endif
						}
					}
					return list;
				}
			}

			/**
			 * The Q3 class based on the database table
			 */
			public class Q3 : Object {
				/**
				 * Get all tuples and fields from database
				 * @param conn The database connection to use
				 * @return Returns a list of TotalCoordinadores
				 */
				public static List<TotalCoordinadores> get_total_coordinators (Connection conn) {
					var res = conn.db.exec ("
SELECT nombreT, TotalCoordinadores
FROM TOTAL_COORDINADORES WHERE (TotalCoordinadores = (SELECT MAX(TotalCoordinadores) FROM TOTAL_COORDINADORES))
					");
					if (res.get_status () != ExecStatus.TUPLES_OK) {
						#if DEBUG
							error (conn.db.get_error_message ());
						#else
							warning (conn.db.get_error_message ());
							return new List<TotalCoordinadores> ();
						#endif
					}

					var wra = new ResultWrapper (res);
					List<TotalCoordinadores> list = new List<TotalCoordinadores> ();
					int n = res.get_n_tuples ();
					for (int i = 0; i < n; i++) {
						try {
							var total = new TotalCoordinadores (0,
								wra.get_string_n (i, "nombreT"),
								wra.get_int_n (i, "TotalCoordinadores")
							);
							list.append (total);
						}
						catch (Error e) {
							#if DEBUG
								error (e.message);
							#else
								warning (e.message);
							#endif
						}
					}
					return list;
				}
			}

			/**
			 * The Q4 class based on the database table
			 */
			public class Q4 : Object {
				/**
				 * Get all tuples and fields from database
				 * @param conn The database connection to use
				 * @return Returns a list of TotalTuristas
				 */
				public static List<TotalTuristas> get_total_tourists (Connection conn) {
					var res = conn.db.exec ("
SELECT nombreT, TotalTuristas FROM TOTAL_TURISTAS WHERE (TotalTuristas = (SELECT MAX(TotalTuristas) FROM TOTAL_TURISTAS))
					");
					if (res.get_status () != ExecStatus.TUPLES_OK) {
						#if DEBUG
							error (conn.db.get_error_message ());
						#else
							warning (conn.db.get_error_message ());
							return new List<TotalTuristas> ();
						#endif
					}

					var wra = new ResultWrapper (res);
					List<TotalTuristas> list = new List<TotalTuristas> ();
					int n = res.get_n_tuples ();
					for (int i = 0; i < n; i++) {
						try {
							var total = new TotalTuristas (0,
								wra.get_string_n (i, "nombreT"),
								wra.get_int_n (i, "TotalTuristas")
							);
							list.append (total);
						}
						catch (Error e) {
							#if DEBUG
								error (e.message);
							#else
								warning (e.message);
							#endif
						}
					}
					return list;
				}
			}

			/**
			 * The Q5 class based on the database table
			 */
			public class Q5 : Object {
				/**
				 * The percentage of cars that were rented and used
				 */
				public float porcentaje { get; set; default = 0; }

				/**
				 * Initialize the Q5 class
				 * @param porcentaje The percentage of cars that were rented and used
				 */
				public Q5 (float porcentaje = 0) {
					this.porcentaje = porcentaje;
				}

				/**
				 * Get all tuples and fields from database
				 * @param conn The database connection to use
				 * @return Returns a list of Q5
				 */
				public static List<Q5> get_percentage (Connection conn) {
					var res = conn.db.exec ("
SELECT
  (cast(T1.totalarriendo AS DECIMAL(3,2)) / cast(T2.totalveh AS DECIMAL(3,2))) AS porcentaje
FROM total_arriendos AS T1, total_vehiculos AS T2
					");
					if (res.get_status () != ExecStatus.TUPLES_OK) {
						#if DEBUG
							error (conn.db.get_error_message ());
						#else
							warning (conn.db.get_error_message ());
							return new List<Q5> ();
						#endif
					}

					var wra = new ResultWrapper (res);
					List<Q5> list = new List<Q5> ();
					int n = res.get_n_tuples ();
					for (int i = 0; i < n; i++) {
						try {
							var percentage = new Q5 (wra.get_float_n (i, "porcentaje"));
							list.append (percentage);
						}
						catch (Error e) {
							#if DEBUG
								error (e.message);
							#else
								warning (e.message);
							#endif
						}
					}
					return list;
				}
			}
		}
	}
}
