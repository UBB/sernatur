/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The Enfermedad class based on the database table
		 */
		public class Enfermedad : Object {
			/**
			 * The id of the illness
			 */
			public uint id_enfermedad { get; set; default = 0; }
			/**
			 * The description of the illness
			 */
			public string descripcion_enfermedad { get; set; default = ""; }

			/**
			 * Initialize the Enfermedad class
			 * @param id_enfermedad The id of the illness
			 * @param descripcion_enfermedad The description of the illness
			 */
			public Enfermedad (uint id_enfermedad = 0, string descripcion_enfermedad = "") {
				this.id_enfermedad = id_enfermedad;
				this.descripcion_enfermedad = descripcion_enfermedad;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Enfermedad
			 * @throws PostgresError If there is a problem with with escaping strings
			 */
			public static List<Enfermedad> get_all_enfermedades (Connection conn) throws PostgresError {
				var res = conn.db.exec ("
SELECT id_enfermedad, descripcion_enfermedad FROM enfermedad
				");
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Enfermedad> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Enfermedad> list = new List<Enfermedad> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var enfermedad = new Enfermedad (wra.get_int_n (i, "id_enfermedad"),
							wra.get_string_n (i, "descripcion_enfermedad")
						);
						list.append (enfermedad);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}
		}
	}
}
