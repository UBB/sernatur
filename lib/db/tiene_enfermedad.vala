/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The TieneEnfermedad class based on the database table
		 */
		public class TieneEnfermedad : Object {
			/**
			 * The tourist
			 */
			public Turista turista { get; set; default = null; }
			/**
			 * The illness
			 */
			public Enfermedad enfermedad { get; set; default = null; }

			/**
			 * Initialize the TieneEnfermedad class
			 * @param turista The tourist
			 * @param enfermedad The illness
			 */
			public TieneEnfermedad (Turista? turista = null, Enfermedad? enfermedad = null) {
				this.turista = turista;
				this.enfermedad = enfermedad;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of TieneEnfermedad
			 */
			public static List<TieneEnfermedad> get_all_tiene_enfermedades (Connection conn) {
				var res = conn.db.exec ("
SELECT T.rut_turista, T.nombre_turista, T.fecha_nacimento,
C.id_contacto, C.telefono_emergencia, C.nombre_emergencia,
E.id_enfermedad, E.descripcion_enfermedad
FROM tiene_enfermedad TE
JOIN turista T ON (TE.rut_turista = T.rut_turista)
JOIN contacto_emergencia C ON (T.id_contacto = C.id_contacto)
JOIN enfermedad E ON (TE.id_enfermedad = E.id_enfermedad)
				");
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<TieneEnfermedad> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<TieneEnfermedad> list = new List<TieneEnfermedad> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var tiene_enfermedad = new TieneEnfermedad (
							new Turista (wra.get_string_n (i, "rut_turista"),
								wra.get_string_n (i, "nombre_turista"),
								wra.get_string_n (i, "fecha_nacimento"),
								new ContactoEmergencia (wra.get_int_n (i, "id_contacto"),
									wra.get_int_n (i, "telefono_emergencia"),
									wra.get_string_n (i, "nombre_emergencia")
								)
							),
							new Enfermedad (wra.get_int_n (i, "id_enfermedad"),
								wra.get_string_n (i, "descripcion_enfermedad")
							)
						);
						list.append (tiene_enfermedad);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}

			/**
			 * Get all tuples and fields from database that are connected with a certain RUN
			 * @param conn The database connection to use
			 * @param run The RUN to search for
			 * @return Returns a list of TieneEnfermedad
			 * @throws PostgresError If there is a problem with with escaping strings
			 */
			public static List<TieneEnfermedad> get_all_tiene_enfermedades_by_run (Connection conn, string run) throws PostgresError {
				var res = conn.db.exec ("
SELECT T.rut_turista, T.nombre_turista, T.fecha_nacimento,
C.id_contacto, C.telefono_emergencia, C.nombre_emergencia,
E.id_enfermedad, E.descripcion_enfermedad
FROM tiene_enfermedad TE
JOIN turista T ON (TE.rut_turista = T.rut_turista)
LEFT JOIN contacto_emergencia C ON (T.id_contacto = C.id_contacto)
JOIN enfermedad E ON (TE.id_enfermedad = E.id_enfermedad)
WHERE (TE.rut_turista = '" + conn.escape (run) + "')
				");
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<TieneEnfermedad> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<TieneEnfermedad> list = new List<TieneEnfermedad> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var tiene_enfermedad = new TieneEnfermedad (
							new Turista (wra.get_string_n (i, "rut_turista"),
								wra.get_string_n (i, "nombre_turista"),
								wra.get_string_n (i, "fecha_nacimento"),
								new ContactoEmergencia (wra.get_int_n (i, "id_contacto"),
									wra.get_int_n (i, "telefono_emergencia"),
									wra.get_string_n (i, "nombre_emergencia")
								)
							),
							new Enfermedad (wra.get_int_n (i, "id_enfermedad"),
								wra.get_string_n (i, "descripcion_enfermedad")
							)
						);
						list.append (tiene_enfermedad);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}

			/**
			 * Insert an illness in the database
			 * @param conn The database connection
			 * @param tiene_enfermedad The illness to insert
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 */
			public static void insert_illness (Connection conn, TieneEnfermedad tiene_enfermedad) throws PostgresError {
				var res = conn.db.exec ("
INSERT INTO tiene_enfermedad
(rut_turista, id_enfermedad)
VALUES
(
'" + conn.escape (tiene_enfermedad.turista.rut_turista) + "',
" + tiene_enfermedad.enfermedad.id_enfermedad.to_string () + "
)
				");
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}

			/**
			 * Delete an illness in the database
			 * @param conn The database connection
			 * @param tiene_enfermedad The illness to delete
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 */
			public static void delete_illness (Connection conn, TieneEnfermedad tiene_enfermedad) throws PostgresError {
				var res = conn.db.exec ("
DELETE FROM tiene_enfermedad
WHERE (
rut_turista = '" + conn.escape (tiene_enfermedad.turista.rut_turista) + "' AND
id_enfermedad = " + tiene_enfermedad.enfermedad.id_enfermedad.to_string () + "
)
				");
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}
		}
	}
}
