/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The Guia class based on the database table
		 */
		public class Guia : Object {
			/**
			 * The RUT of the guide
			 */
			public string rut_guia { get; set; default = ""; }
			/**
			 * The name of the guide
			 **/
			public string nombre_guia { get; set; default = ""; }
			/**
			 * The stree name
			 */
			public string calle { get; set; default = ""; }
			/**
			 * The street number
			 */
			public uint numero { get; set; default = 0; }
			/**
			 * The city
			 */
			public Ciudad ciudad { get; set; default = null; }

			/**
			 * Initialize the Guia class
			 * @param rut_guia The RUT of the guide
			 * @param nombre_guia The name of the guide
			 * @param calle The street name
			 * @param numero The street number
			 * @param ciudad The city
			 */
			public Guia (string rut_guia = "", string nombre_guia = "", string calle = "", uint numero = 0, Ciudad? ciudad = null) {
				this.rut_guia = rut_guia;
				this.nombre_guia = nombre_guia;
				this.calle = calle;
				this.numero = numero;
				this.ciudad = ciudad;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Guia
			 * @throws PostgresError If there is a problem with with escaping strings
			 */
			public static List<Guia> get_all_guias (Connection conn) throws PostgresError {
				var res = conn.db.exec ("
SELECT G.rut_guia, G.nombre_guia, G.calle, G.numero,
C.id_ciudad, C.nombre_ciudad,
R.id_region, R.nombre_region
FROM guia G
JOIN ciudad C ON (G.id_ciudad = C.id_ciudad)
JOIN region R ON (C.id_region = R.id_region)
				");
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Guia> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Guia> list = new List<Guia> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var guia = new Guia	(wra.get_string_n (i, "rut_guia"),
							wra.get_string_n (i, "nombre_guia"),
							wra.get_string_n (i, "calle"),
							wra.get_int_n (i, "numero"),
							new Ciudad (wra.get_int_n (i, "id_ciudad"),
								wra.get_string_n (i, "nombre_ciudad"),
								new Region (wra.get_int_n (i, "id_region"),
									wra.get_string_n (i, "nombre_region")
								)
							)
						);
						list.append (guia);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}

			/**
			 * Get a tuple and fields from database
			 * @param conn The database connection to use
			 * @return Returns a Guia
			 * @throws PostgresError If there is a problem with with escaping strings
			 */
			public static Guia? get_guia_by_run (Connection conn, string run) throws PostgresError {
				var res = conn.db.exec ("
SELECT G.rut_guia, G.nombre_guia, G.calle, G.numero,
C.id_ciudad, C.nombre_ciudad,
R.id_region, R.nombre_region
FROM guia G
JOIN ciudad C ON (G.id_ciudad = C.id_ciudad)
JOIN region R ON (C.id_region = R.id_region)
WHERE rut_guia = '" + conn.escape (run) + "'
				");
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return null;
					#endif
				}

				var wra = new ResultWrapper (res);
				Guia guia = null;
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						guia = new Guia	(wra.get_string_n (i, "rut_guia"),
							wra.get_string_n (i, "nombre_guia"),
							wra.get_string_n (i, "calle"),
							wra.get_int_n (i, "numero"),
							new Ciudad (wra.get_int_n (i, "id_ciudad"),
								wra.get_string_n (i, "nombre_ciudad"),
								new Region (wra.get_int_n (i, "id_region"),
									wra.get_string_n (i, "nombre_region")
								)
							)
						);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return guia;
			}

			/**
			 * Insert a staff member in the database
			 * @param conn The database connection
			 * @param guia The staff member to insert
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 */
			public static void insert_illness (Connection conn, Guia guia) throws PostgresError {
				var res = conn.db.exec ("
INSERT INTO guia
(rut_guia, nombre_guia, calle, numero, id_ciudad)
VALUES
(
'" + conn.escape (guia.rut_guia) + "',
'" + conn.escape (guia.nombre_guia) + "',
'" + conn.escape (guia.calle) + "',
" + guia.numero.to_string () + ",
" + guia.ciudad.id_ciudad.to_string () + "
)
				");
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}

			/**
			 * Delete a staff member in the database
			 * @param conn The database connection
			 * @param guia The staff member to delete
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 */
			public static void delete_staff (Connection conn, Guia guia) throws PostgresError {
				var res = conn.db.exec ("
DELETE FROM guia
WHERE (
rut_guia = '" + conn.escape (guia.rut_guia) + "'
)
				");
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}
		}
	}
}
