/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The Ciudad class based on the database table
		 */
		public class Ciudad : Object {
			/**
			 * The id of the city
			 */
			public uint id_ciudad { get; set; default = 0; }
			/**
			 * The name of the city
			 */
			public string nombre_ciudad { get; set; default = ""; }
			/**
			 * The region
			 */
			public Region region { get; set; default = null; }

			/**
			 * Initialize the Ciudad class
			 * @param id_ciudad The id of the city
			 * @param nombre_ciudad The name of the city
			 * @param region The region
			 * */
			public Ciudad (uint id_ciudad = 0, string nombre_ciudad = "", Region? region = null) {
				this.id_ciudad = id_ciudad;
				this.nombre_ciudad = nombre_ciudad;
				this.region = region;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Ciudad
			 * @throws PostgresError If there is a problem with with escaping strings
			 */
			public static List<Ciudad> get_all_ciudades (Connection conn) throws PostgresError {
				var res = conn.db.exec (Query.get_query (conn, Query.Type.SELECT_ALL_CITIES, null));
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Ciudad> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Ciudad> list = new List<Ciudad> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var ciudad = new Ciudad (wra.get_int_n (i, "id_ciudad"),
							wra.get_string_n (i, "nombre_ciudad"),
							new Region (wra.get_int_n (i, "id_region"),
								wra.get_string_n (i, "nombre_region")
							)
						);
						list.append (ciudad);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}

			/**
			 * Get all tuples and fields from database that are within a certain region
			 * @param conn The database connection to use
			 * @param region_id The id of the region to filter that results
			 * @return Returns a list of Ciudad
			 * @throws PostgresError If there is a problem with with escaping strings
			 */
			public static List<Ciudad> get_all_ciudades_in_region (Connection conn, uint region_id) throws PostgresError {
				var res = conn.db.exec (Query.get_query (conn, Query.Type.SELECT_ALL_CITIES_BY_REGION, region_id));
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Ciudad> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Ciudad> list = new List<Ciudad> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var ciudad = new Ciudad (wra.get_int_n (i, "id_ciudad"),
							wra.get_string_n (i, "nombre_ciudad"),
							new Region (wra.get_int_n (i, "id_region"),
								wra.get_string_n (i, "nombre_region")
							)
						);
						list.append (ciudad);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}

			/**
			 * Insert a city in the database
			 * @param conn The database connection
			 * @param ciudad The city to insert
			 * @return Returns the id of the tuple inserted
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static uint insert_city (Connection conn, Ciudad ciudad) throws PostgresError, DBError {
				if (ciudad.id_ciudad != 0) {
					throw new DBError.INVALID_VALUE (_ ("The id of the city is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.INSERT_CITY, ciudad));
				// This uses TUPLES_OK because it returns a result which is the id of the inserted city
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}

				var wra = new ResultWrapper (res);
				try {
					return wra.get_int_n (0, "id_ciudad");
				}
				catch (Error e) {
					#if DEBUG
						error (e.message);
					#else
						warning (e.message);
						return 0;
					#endif
				}
			}
		}
	}
}
