/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		using Postgres;
		using Wrapper;

		/**
		 * The Lugar class based on the database table
		 */
		public class Lugar : Object {
			/**
			 * The id of the place
			 */
			public uint id_lugar { get; set; default = 0; }
			/**
			 * The name of the place
			 */
			public string nombre_lugar { get; set; default = ""; }
			/**
			 * The cost to enter
			 */
			public uint valor_entrada { get; set; default = 0; }
			/**
			 * The level of difficulty
			 */
			public uint nivel { get; set; default = 0; }
			/**
			 * The city
			 */
			public Ciudad ciudad { get; set; default = null; }

			/**
			 * Initialize the Lugar class
			 * @param id_lugar The id of the place
			 * @param nombre_lugar The name of the place
			 * @param valor_entrada The cost to enter
			 * @param nivel The level of difficulty
			 * @param ciudad The city
			 */
			public Lugar (uint id_lugar = 0, string nombre_lugar = "", uint valor_entrada = 0, uint nivel = 0, Ciudad? ciudad = null) {
				this.id_lugar = id_lugar;
				this.nombre_lugar = nombre_lugar;
				this.valor_entrada = valor_entrada;
				this.nivel = nivel;
				this.ciudad = ciudad;
			}

			/**
			 * Get all tuples and fields from database
			 * @param conn The database connection to use
			 * @return Returns a list of Lugar
			 * @throws PostgresError If there is a problem with with escaping strings
			 */
			public static List<Lugar> get_all_lugares (Connection conn) throws PostgresError {
				var res = conn.db.exec (Query.get_query (conn, Query.Type.SELECT_ALL_PLACES, null));
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
						return new List<Lugar> ();
					#endif
				}

				var wra = new ResultWrapper (res);
				List<Lugar> list = new List<Lugar> ();
				int n = res.get_n_tuples ();
				for (int i = 0; i < n; i++) {
					try {
						var lugar = new Lugar (wra.get_int_n (i, "id_lugar"),
							wra.get_string_n (i, "nombre_lugar"),
							wra.get_int_n (i, "valor_entrada"),
							wra.get_int_n (i, "nivel"),
							new Ciudad (wra.get_int_n (i, "id_ciudad"),
								wra.get_string_n (i, "nombre_ciudad"),
								new Region(wra.get_int_n (i, "id_region"),
									wra.get_string_n (i, "nombre_region")
								)
							)
						);
						list.append (lugar);
					}
					catch (Error e) {
						#if DEBUG
							error (e.message);
						#else
							warning (e.message);
						#endif
					}
				}
				return list;
			}

			/**
			 * Update a place in the database
			 * @param conn The database connection
			 * @param lugar The place to update
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static void update_place (Connection conn, Lugar lugar) throws PostgresError, DBError {
				if (lugar.id_lugar == 0) {
					throw new DBError.INVALID_VALUE (_ ("The id of the place is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.UPDATE_PLACE, lugar));
				if (res.get_status () != ExecStatus.COMMAND_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}
			}

			/**
			 * Insert a place in the database
			 * @param conn The database connection
			 * @param lugar The place to insert
			 * @return Returns the id of the tuple inserted
			 * @throws PostgresError Thrown if there is a problem with escaping strings
			 * @throws DBError Thrown if an invalid value is passed
			 */
			public static uint insert_place (Connection conn, Lugar lugar) throws PostgresError, DBError {
				if (lugar.id_lugar != 0) {
					throw new DBError.INVALID_VALUE (_ ("The id of the place is invalid!"));
				}
				var res = conn.db.exec (Query.get_query (conn, Query.Type.INSERT_PLACE, lugar));
				// This uses TUPLES_OK because it returns a result which is the id of the inserted tour
				if (res.get_status () != ExecStatus.TUPLES_OK) {
					#if DEBUG
						error (conn.db.get_error_message ());
					#else
						warning (conn.db.get_error_message ());
					#endif
				}

				var wra = new ResultWrapper (res);
				try {
					return wra.get_int_n (0, "id_lugar");
				}
				catch (Error e) {
					#if DEBUG
						error (e.message);
					#else
						warning (e.message);
						return 0;
					#endif
				}
			}
		}
	}
}
