/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		/**
		 * The queries to use to work in the database
		 */
		public class Query < T > : Object {
			public enum Type {
				SELECT_ALL_CITIES,
				SELECT_ALL_CITIES_BY_REGION,
				INSERT_CITY,
				SELECT_ALL_REGIONS,
				INSERT_REGION,
				SELECT_ALL_TOURS,
				INSERT_TOUR,
				UPDATE_TOUR,
				DELETE_TOUR,
				SELECT_ALL_PLACES,
				INSERT_PLACE,
				UPDATE_PLACE,
				SELECT_ALL_ASSOCIATED,
				SELECT_ALL_ASSOCIATED_BY_TOUR,
				INSERT_ASSOCIATED,
				DELETE_ASSOCIATED,
				SELECT_ALL_VEHICLE_REQUIRED,
				SELECT_VEHICLE_REQUIRED_BY_TOUR,
				INSERT_VEHICLE_REQUIRED,
				UPDATE_VEHICLE_REQUIRED,
				DELETE_VEHICLE_REQUIRED,
				SELECT_ALL_TOURISTS,
				SELECT_TOURIST_BY_RUN,
				INSERT_TOURIST,
				UPDATE_TOURIST,
				DELETE_TOURIST
			}

			/**
			 * Return a query to be run
			 * @param conn The connection which is used for escaping strings
			 * @param type Which type of query is required
			 * @param t Generic variable whose use depends on the query
			 * @param t2 Generic variable whose use depends on the query
			 * @return Returns a string which contains a query
			 * @throws PostgresError Thrown if escaping strings fails
			 */
			public static string get_query (Connection conn, Type type, T? t = null, T? t2 = null) throws PostgresError {
				switch (type) {
					case SELECT_ALL_CITIES:
						return "
SELECT C.id_ciudad, C.nombre_ciudad,
R.id_region, R.nombre_region
FROM ciudad C
JOIN region R ON (C.id_region = R.id_region)";

					case SELECT_ALL_CITIES_BY_REGION:
						uint region_id = (uint) t;
						return "
SELECT C.id_ciudad, C.nombre_ciudad,
R.id_region, R.nombre_region
FROM ciudad C
JOIN region R ON (C.id_region = R.id_region)
WHERE (R.id_region = " + region_id.to_string () + ")";

					case INSERT_CITY:
						Ciudad ciudad = (Ciudad) t;
						return "
INSERT INTO ciudad
(nombre_ciudad, id_region)
VALUES
(
'" + conn.escape (ciudad.nombre_ciudad) + "',
" + ciudad.region.id_region.to_string () + "
)
RETURNING id_ciudad";

					case SELECT_ALL_REGIONS:
						return "
SELECT id_region, nombre_region FROM region";

					case INSERT_REGION:
						Region region = (Region) t;
						return "
INSERT INTO region
(nombre_region)
VALUES
(
'" + conn.escape (region.nombre_region) + "'
)
RETURNING id_region";

					case SELECT_ALL_TOURS:
						return "
SELECT T.id_tour, T.nombre_tour, T.costo_indiv, T.costo_grupal, T.minima_personas,
C.id_ciudad, C.nombre_ciudad,
R.id_region, R.nombre_region
FROM tour T
JOIN ciudad C ON (T.id_ciudad = C.id_ciudad)
JOIN region R ON (C.id_region = R.id_region)";

					case UPDATE_TOUR:
						Tour tour = (Tour) t;
						return "
UPDATE tour SET
	nombre_tour = '" + conn.escape (tour.nombre_tour) + "',
	costo_indiv = " + tour.costo_indiv.to_string () + ",
	costo_grupal = " + tour.costo_grupal.to_string () + ",
	minima_personas = " + tour.minima_personas.to_string () + ",
	id_ciudad = " + tour.ciudad.id_ciudad.to_string () + "
WHERE id_tour = " + tour.id_tour.to_string ();

					case INSERT_TOUR:
						Tour tour = (Tour) t;
						return "
INSERT INTO tour
(nombre_tour, costo_indiv, costo_grupal, minima_personas, id_ciudad)
VALUES
(
'" + conn.escape (tour.nombre_tour) + "',
" + tour.costo_indiv.to_string () + ",
" + tour.costo_grupal.to_string () + ",
" + tour.minima_personas.to_string () + ",
" + tour.ciudad.id_ciudad.to_string () + "
)
RETURNING id_tour";

					case DELETE_TOUR:
						Tour tour = (Tour) t;
						return "
DELETE FROM tour
WHERE id_tour = " + tour.id_tour.to_string ();

					case SELECT_ALL_PLACES:
						return "
SELECT L.id_lugar, L.nombre_lugar, L.valor_entrada, L.nivel,
C.id_ciudad, C.nombre_ciudad,
R.id_region, R.nombre_region
FROM lugar L
JOIN ciudad C ON (L.id_ciudad = C.id_ciudad)
JOIN region R ON (C.id_region = R.id_region)";

					case UPDATE_PLACE:
						Lugar lugar = (Lugar) t;
						return "
UPDATE lugar SET
nombre_lugar = '" + conn.escape (lugar.nombre_lugar) + "',
valor_entrada = " + lugar.valor_entrada.to_string () + ",
nivel = " + lugar.nivel.to_string () + ",
id_ciudad = " + lugar.ciudad.id_ciudad.to_string () + "
WHERE id_lugar = " + lugar.id_lugar.to_string ();

					case INSERT_PLACE:
						Lugar lugar = (Lugar) t;
						return "
INSERT INTO lugar
(nombre_lugar, valor_entrada, nivel, id_ciudad)
VALUES
(
'" + conn.escape (lugar.nombre_lugar) + "',
" + lugar.valor_entrada.to_string () + ",
" + lugar.nivel.to_string () + ",
" + lugar.ciudad.id_ciudad.to_string () + "
)
RETURNING id_lugar";

					case SELECT_ALL_ASSOCIATED:
						return "
SELECT A.fecha_llegada, A.hora_llegada, A.fecha_salida, A.hora_salida,
T.id_tour, T.nombre_tour, T.costo_indiv, T.costo_grupal, T.minima_personas,
C.id_ciudad, C.nombre_ciudad,
R.id_region, R.nombre_region,
L.id_lugar, L.nombre_lugar, L.valor_entrada, L.nivel,
C2.id_ciudad AS id_ciudad_lugar, C2.nombre_ciudad AS nombre_ciudad_lugar,
R2.id_region AS id_region_lugar, R2.nombre_region AS nombre_region_lugar
FROM asociado A
JOIN tour T ON (A.id_tour = T.id_tour)
JOIN ciudad C ON (T.id_ciudad = C.id_ciudad)
JOIN region R ON (C.id_region = R.id_region)
JOIN lugar L ON (A.id_lugar = L.id_lugar)
JOIN ciudad C2 ON (L.id_ciudad = C2.id_ciudad)
JOIN region R2 ON (C2.id_region = R2.id_region)";

					case SELECT_ALL_ASSOCIATED_BY_TOUR:
						Tour tour = (Tour) t;
							return "
SELECT A.fecha_llegada, A.hora_llegada, A.fecha_salida, A.hora_salida,
T.id_tour, T.nombre_tour, T.costo_indiv, T.costo_grupal, T.minima_personas,
C.id_ciudad, C.nombre_ciudad,
R.id_region, R.nombre_region,
L.id_lugar, L.nombre_lugar, L.valor_entrada, L.nivel,
C2.id_ciudad AS id_ciudad_lugar, C2.nombre_ciudad AS nombre_ciudad_lugar,
R2.id_region AS id_region_lugar, R2.nombre_region AS nombre_region_lugar
FROM asociado A
JOIN tour T ON (A.id_tour = T.id_tour)
JOIN ciudad C ON (T.id_ciudad = C.id_ciudad)
JOIN region R ON (C.id_region = R.id_region)
JOIN lugar L ON (A.id_lugar = L.id_lugar)
JOIN ciudad C2 ON (L.id_ciudad = C2.id_ciudad)
JOIN region R2 ON (C2.id_region = R2.id_region)
WHERE t.id_tour = " + tour.id_tour.to_string ();

					case INSERT_ASSOCIATED:
						Asociado asociado = (Asociado) t;
						return "
INSERT INTO asociado
(id_tour, id_lugar, fecha_llegada, hora_llegada, fecha_salida, hora_salida)
VALUES
(
" + asociado.tour.id_tour.to_string () + ",
" + asociado.lugar.id_lugar.to_string () + ",
'" + conn.escape (asociado.fecha_llegada) + "',
'" + conn.escape (asociado.hora_llegada) + "',
'" + conn.escape (asociado.fecha_salida) + "',
'" + conn.escape (asociado.hora_salida) + "'
)";

					case DELETE_ASSOCIATED:
						Asociado asociado = (Asociado) t;
						return "
DELETE FROM associated
WHERE (
id_tour = " + asociado.tour.id_tour.to_string () + " AND
id_lugar = " + asociado.lugar.id_lugar.to_string () + "
)";

					case SELECT_ALL_VEHICLE_REQUIRED:
						return "
SELECT R.chofer,
T.id_tour, T.nombre_tour, T.costo_indiv, T.costo_grupal, T.minima_personas,
C.id_ciudad, C.nombre_ciudad,
R2.id_region, R2.nombre_region,
V.patente, V.ano_vehiculo, V.marca, V.capacidad
FROM requerir_auto R
JOIN tour T ON (R.id_tour = T.id_tour)
JOIN ciudad C ON (T.id_ciudad = C.id_ciudad)
JOIN region R2 ON (C.id_region = R2.id_region)
JOIN vehiculo V ON (R.patente = V.patente)";

					case SELECT_VEHICLE_REQUIRED_BY_TOUR:
						Tour tour = (Tour) t;
						return "
SELECT R.chofer,
V.patente, V.ano_vehiculo, V.marca, V.capacidad
FROM requerir_auto R
JOIN vehiculo V ON (R.patente = V.patente)
WHERE (
id_tour = " + tour.id_tour.to_string () + "
)";

					case INSERT_VEHICLE_REQUIRED:
						RequerirAuto requerir_auto = (RequerirAuto) t;
						return "
INSERT INTO requerir_auto
(id_tour, patente, chofer)
VALUES
(
" + requerir_auto.tour.id_tour.to_string () + ",
'" + conn.escape (requerir_auto.vehiculo.patente) + "',
'" + conn.escape (requerir_auto.chofer) + "'
)";

					case UPDATE_VEHICLE_REQUIRED:
						RequerirAuto requerir_auto = (RequerirAuto) t;
						return "
UPDATE requerir_auto SET
patente = '" + conn.escape (requerir_auto.vehiculo.patente) + "',
chofer = '" + conn.escape (requerir_auto.chofer) + "'
WHERE id_tour = " + requerir_auto.tour.id_tour.to_string ();

					case DELETE_VEHICLE_REQUIRED:
						Tour tour = (Tour) t;
						return "
DELETE FROM requerir_auto
WHERE (
id_tour = " + tour.id_tour.to_string () + "
)";

					case SELECT_ALL_TOURISTS:
						return "
SELECT T.rut_turista, T.nombre_turista, T.fecha_nacimento,
C.id_contacto, C.telefono_emergencia, C.nombre_emergencia
FROM turista T
LEFT JOIN contacto_emergencia C ON (T.id_contacto = C.id_contacto)";

					case SELECT_TOURIST_BY_RUN:
						string run = (string) t;
						return "
SELECT T.rut_turista, T.nombre_turista, T.fecha_nacimento,
C.id_contacto, C.telefono_emergencia, C.nombre_emergencia
FROM turista T
LEFT JOIN contacto_emergencia C ON (T.id_contacto = C.id_contacto)
WHERE rut_turista = '" + conn.escape (run) + "'";

					case INSERT_TOURIST:
						Turista turista = (Turista) t;
						return "
INSERT INTO turista
(rut_turista, nombre_turista, fecha_nacimento)
VALUES
(
'" + conn.escape (turista.rut_turista) + "'',
'" + conn.escape (turista.nombre_turista) + "',
'" + conn.escape (turista.fecha_nacimento) + "'
)";

					case UPDATE_TOURIST:
						Turista turista = (Turista) t;
						return "
UPDATE turista SET
rut_turista = '" + conn.escape (turista.rut_turista) + "',
nombre_turista = '" + conn.escape (turista.nombre_turista) + "',
fecha_nacimento = '" + conn.escape (turista.fecha_nacimento) + "'
WHERE rut_turista = '" + conn.escape ((string) t2) + "'";

					case DELETE_TOURIST:
						Turista turista = (Turista) t;
						return "
DELETE FROM turista
WHERE (
rut_turista = '" + conn.escape (turista.rut_turista) + "'
)";

					default:
						return "";
				}
			}
		}
	}
}
