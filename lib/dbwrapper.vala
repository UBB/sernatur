/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace LibSernatur {
	namespace DB {
		/**
		 * This is a wrapper for Postgres that allows easier use of the database
		 */
		namespace Wrapper {
			using Postgres;

			/**
			 * The errors that can be thrown by fields in the database
			 */
			public errordomain Field {
				/**
				 * Field was not found in table
				 */
				NOTFOUND
			}

			/**
			 * This is a wrapper for Postgresql results
			 */
			public class ResultWrapper : Object {
				/**
				 * The result that this is wrapped around
				 */
				public unowned Result result { get; set; default = null; }

				/**
				 * Initialize the result wrapper
				 * @param result The result to wrap around
				 */
				public ResultWrapper (Result? result = null) {
					this.result = result;
				}

				/**
				 * Return the string value based on tuple and field number
				 * @param tup_num The tuple number
				 * @param field_num The field number
				 * @return Returns a string of the value
				 */
				public string get_string_v (int tup_num, int field_num) {
					return result.get_value (tup_num, field_num);
				}

				/**
				 * Return the int value based on tuple and field number
				 * @param tup_num The tuple number
				 * @param field_num The field number
				 * @return Returns an int of the value
				 */
				public int get_int_v (int tup_num, int field_num) {
					return int.parse (result.get_value (tup_num, field_num));
				}

				/**
				 * Return the float value based on tuple and field number
				 * @param tup_num The tuple number
				 * @param field_num The field number
				 * @return Returns a float of the value
				 */
				public float get_float_v (int tup_num, int field_num) {
					return float.parse(result.get_value (tup_num, field_num));
				}

				/**
				 * Return the double value based on tuple and field number
				 * @param tup_num The tuple number
				 * @param field_num The field number
				 * @return Returns a double of the value
				 */
				public double get_double_v (int tup_num, int field_num) {
					return double.parse (result.get_value (tup_num, field_num));
				}

				/**
				 * Return the string value based on tuple number and field name
				 * @param tup_num The tuple number
				 * @param name The field name
				 * @return Returns a string of the value
				 */
				public string get_string_n (int tup_num, string name) throws Field {
					return get_string_v (tup_num, check_field_found (result.get_field_number (name), name));
				}

				/**
				 * Return the int value based on tuple number and field name
				 * @param tup_num The tuple number
				 * @param name The field name
				 * @return Returns an int of the value
				 */
				public int get_int_n (int tup_num, string name) throws Field {
					return get_int_v (tup_num, check_field_found (result.get_field_number (name), name));
				}

				/**
				 * Return the float value based on tuple number and field name
				 * @param tup_num The tuple number
				 * @param name The field name
				 * @return Returns a float of the value
				 */
				public float get_float_n (int tup_num, string name) throws Field {
					return get_float_v (tup_num, check_field_found (result.get_field_number (name), name));
				}

				/**
				 * Return the double value based on tuple number and field name
				 * @param tup_num The tuple number
				 * @param name The field name
				 * @return Returns a double of the value
				 */
				public double get_double_n (int tup_num, string name) throws Field {
					return get_double_v (tup_num, check_field_found (result.get_field_number (name), name));
				}

				/**
				 * Check if the field is found in the results, if not throw a Field error
				 * @param field_num The field number to check for
				 * @param name The field name we were looking for
				 * @return The field number that corresponds to the field name
				 */
				private int check_field_found (int field_num, string name) throws Field {
					if (field_num == -1) {
						throw new Field.NOTFOUND (_ ("The field %s was not found in the query results!"), name);
					}
					return field_num;
				}
			}
		}
	}
}
