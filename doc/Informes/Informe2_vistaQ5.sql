CREATE VIEW TOTAL_ARRIENDOS(TotalArriendo)
AS (SELECT COUNT(A.patente)
  FROM arrienda A
  JOIN requerir_auto R ON (R.patente = A.patente)
  JOIN tour T ON (T.id_tour = R.id_tour)
  JOIN asociado A2 ON (T.id_tour = A2.id_tour)
  WHERE (A2.fecha_llegada BETWEEN '2018-01-01' AND '2018-02-28')
);

CREATE VIEW TOTAL_VEHICULOS(TotalVeh)
AS (SELECT COUNT(patente)
  FROM vehiculo
);

SELECT
  (cast(T1.totalarriendo AS DECIMAL(3,2)) / cast(T2.totalveh AS DECIMAL(3,2))) AS porcentaje
FROM total_arriendos AS T1, total_vehiculos AS T2;
