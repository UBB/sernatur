CREATE VIEW REGIONES_SINDESCUENTO(nombreRegion, cantidad)
AS (
  SELECT nombre_region, COUNT(R2.rut_turista)
  FROM region R
    JOIN ciudad C ON (C.id_region = R.id_region)
    JOIN tour T ON (T.id_ciudad = C.id_ciudad)
    JOIN realiza R2 ON (R2.id_tour = T.id_tour)
  WHERE (
    EXISTS(SELECT fecha_llegada
	  FROM asociado
      WHERE (id_tour = T.id_tour AND fecha_llegada BETWEEN '2018-11-01' AND '2018-11-30')
    ) AND
    R2.id_descuento = 2
  )
  GROUP BY (R.nombre_region)
);

SELECT nombreRegion, cantidad FROM REGIONES_SINDESCUENTO ORDER BY cantidad DESC;
