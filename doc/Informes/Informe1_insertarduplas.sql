INSERT INTO contacto_emergencia (telefono_emergencia, nombre_emergencia) VALUES ('56912345678', 'Chris Cromer');
INSERT INTO contacto_emergencia (telefono_emergencia, nombre_emergencia) VALUES ('56913334578', 'John Cromer');
INSERT INTO contacto_emergencia (telefono_emergencia, nombre_emergencia) VALUES ('56913984347', 'Tammy Cromer');

INSERT INTO enfermedad (descripcion_enfermedad) VALUES ('Heart Disease');
INSERT INTO enfermedad (descripcion_enfermedad) VALUES ('Diabetes');
INSERT INTO enfermedad (descripcion_enfermedad) VALUES ('Lung Cancer');

INSERT INTO empresa (rut_empresa, nombre_empresa, contacto, telefono) VALUES ('566034578', 'Hurtz', 'John Jacob', '56948904446');
INSERT INTO empresa (rut_empresa, nombre_empresa, contacto, telefono) VALUES ('56404537k', 'Rent-a-Car', 'Jim Smith', '56948904446');
INSERT INTO empresa (rut_empresa, nombre_empresa, contacto, telefono) VALUES ('566003458', 'Car Rentals Dot Com', 'Tony Stark', '56948904446');

INSERT INTO guia (rut_guia, nombre_guia) VALUES ('266304578', 'John Smith');
INSERT INTO guia (rut_guia, nombre_guia) VALUES ('26640457k', 'Jack Offer');
INSERT INTO guia (rut_guia, nombre_guia) VALUES ('26644457k', 'Mike Toga');

INSERT INTO especialidad (descripcion_especialidad) VALUES ('Climbing');
INSERT INTO especialidad (descripcion_especialidad) VALUES ('History');
INSERT INTO especialidad (descripcion_especialidad) VALUES ('Astronomy');

INSERT INTO categoria (descripcion_categoria) VALUES ('Coordinador');
INSERT INTO categoria (descripcion_categoria) VALUES ('Speaker');
INSERT INTO categoria (descripcion_categoria) VALUES ('Expert');

INSERT INTO vehiculo (patente, ano_vehiculo, marca, capacidad, chofer) VALUES ('as45fe', 2017, 'Chevy', 4, 'Tony');
INSERT INTO vehiculo (patente, ano_vehiculo, marca, capacidad, chofer) VALUES ('lj58bv', 2002, 'Mazda', 20, 'Jimmy');
INSERT INTO vehiculo (patente, ano_vehiculo, marca, capacidad, chofer) VALUES ('49fd38', 2001, 'Subaru', 10, 'Jake');

INSERT INTO region (nombre_region) VALUES ('Florida');
INSERT INTO region (nombre_region) VALUES ('Alabama');
INSERT INTO region (nombre_region) VALUES ('Arizona');

INSERT INTO ciudad (nombre_ciudad, id_region) VALUES ('Pensacola', 1);
INSERT INTO ciudad (nombre_ciudad, id_region) VALUES ('Montgomery', 2);
INSERT INTO ciudad (nombre_ciudad, id_region) VALUES ('Pheonix', 3);

INSERT INTO tour (nombre_tour, costo_indiv, costo_grupal, minima_personas, id_ciudad) VALUES ('Los Rios', 200000, 150000, 1, 1);
INSERT INTO tour (nombre_tour, costo_indiv, costo_grupal, minima_personas, id_ciudad) VALUES ('Valle de la Luna', 300000, 120000, 10, 2);
INSERT INTO tour (nombre_tour, costo_indiv, costo_grupal, minima_personas, id_ciudad) VALUES ('Peru', 400000, 110000, 20, 3);

INSERT INTO lugar (nombre_lugar, valor_entrada, nivel, id_ciudad) VALUES ('El Bio Bio', 0, 0, 1);
INSERT INTO lugar (nombre_lugar, valor_entrada, nivel, id_ciudad) VALUES ('El Bio Bio', 10000, 2, 2);
INSERT INTO lugar (nombre_lugar, valor_entrada, nivel, id_ciudad) VALUES ('El Bio Bio', 30000, 5, 3);

INSERT INTO turista (rut_turista, nombre_turista, fecha_nacimento, id_contacto) VALUES ('226634572', 'Jack Asserton', '1985-02-23', 1);
INSERT INTO turista (rut_turista, nombre_turista, fecha_nacimento, id_contacto) VALUES ('194333573', 'Tommy Gunner', '1990-06-02', 2);
INSERT INTO turista (rut_turista, nombre_turista, fecha_nacimento, id_contacto) VALUES ('17398380k', 'Jimmy Johnson', '1955-01-01', 3);

INSERT INTO tiene_enfermedad (rut_turista, id_enfermedad) VALUES ('226634572', 1);
INSERT INTO tiene_enfermedad (rut_turista, id_enfermedad) VALUES ('194333573', 2);
INSERT INTO tiene_enfermedad (rut_turista, id_enfermedad) VALUES ('17398380k', 3);

INSERT INTO compra (id_tour, rut_turista, valor) VALUES (1, '226634572', 150000);
INSERT INTO compra (id_tour, rut_turista, valor) VALUES (2, '194333573', 120000);
INSERT INTO compra (id_tour, rut_turista, valor) VALUES (3, '17398380k', 110000);

INSERT INTO realiza (id_tour, rut_turista) VALUES (1, '226634572');
INSERT INTO realiza (id_tour, rut_turista) VALUES (1, '226634572');
INSERT INTO realiza (id_tour, rut_turista) VALUES (1, '226634572');

INSERT INTO arrienda (patente, rut_empresa, precio, fecha_devolucion) VALUES ('as45fe', '566034578', 120000, '2018-11-29');
INSERT INTO arrienda (patente, rut_empresa, precio, fecha_devolucion) VALUES ('lj58bv', '56404537k', 100000, '2018-11-29');
INSERT INTO arrienda (patente, rut_empresa, precio, fecha_devolucion) VALUES ('49fd38', '566003458', 150000, '2018-11-29');

INSERT INTO posee (rut_guia, id_especialidad, nivel_especialidad) VALUES ('266304578', 1, 0);
INSERT INTO posee (rut_guia, id_especialidad, nivel_especialidad) VALUES ('26640457k', 2, 3);
INSERT INTO posee (rut_guia, id_especialidad, nivel_especialidad) VALUES ('26644457k', 3, 5);

INSERT INTO direccion (rut_guia, id_ciudad, calle, numero) VALUES ('266304578', 1, 'Edison Dr.', 214);
INSERT INTO direccion (rut_guia, id_ciudad, calle, numero) VALUES ('26640457k', 1, 'Jackson Ave.', 1023);
INSERT INTO direccion (rut_guia, id_ciudad, calle, numero) VALUES ('26644457k', 1, 'Madison Way', 342);

INSERT INTO participa (id_tour, rut_guia, id_categoria) VALUES (1, '266304578', 1);
INSERT INTO participa (id_tour, rut_guia, id_categoria) VALUES (2, '26640457k', 1);
INSERT INTO participa (id_tour, rut_guia, id_categoria) VALUES (3, '26644457k', 1);

INSERT INTO asociado (id_tour, id_lugar, fecha_llegada, hora_llegada, fecha_salida, hora_salida) VALUES (1, 1, '2018-09-29', '04:15', '2018-09-30', '05:15');
INSERT INTO asociado (id_tour, id_lugar, fecha_llegada, hora_llegada, fecha_salida, hora_salida) VALUES (2, 2, '2018-10-29', '09:35', '2018-10-30', '10:35');
INSERT INTO asociado (id_tour, id_lugar, fecha_llegada, hora_llegada, fecha_salida, hora_salida) VALUES (3, 3, '2018-11-29', '16:40', '2018-11-30', '19:10');
