INSERT INTO contacto_emergencia (telefono_emergencia, nombre_emergencia) VALUES ('56912345678', 'Chris Cromer');
INSERT INTO contacto_emergencia (telefono_emergencia, nombre_emergencia) VALUES ('56913334578', 'John Cromer');
INSERT INTO contacto_emergencia (telefono_emergencia, nombre_emergencia) VALUES ('56913984347', 'Tammy Cromer');

INSERT INTO enfermedad (descripcion_enfermedad) VALUES ('Heart Disease');
INSERT INTO enfermedad (descripcion_enfermedad) VALUES ('Diabetes');
INSERT INTO enfermedad (descripcion_enfermedad) VALUES ('Lung Cancer');

INSERT INTO empresa (rut_empresa, nombre_empresa, contacto, telefono) VALUES ('566034578', 'Hurtz', 'John Jacob', '56948904446');
INSERT INTO empresa (rut_empresa, nombre_empresa, contacto, telefono) VALUES ('56404537k', 'Rent-a-Car', 'Jim Smith', '56948904446');
INSERT INTO empresa (rut_empresa, nombre_empresa, contacto, telefono) VALUES ('566003458', 'Car Rentals Dot Com', 'Tony Stark', '56948904446');

INSERT INTO especialidad (descripcion_especialidad) VALUES ('Climbing');
INSERT INTO especialidad (descripcion_especialidad) VALUES ('History');
INSERT INTO especialidad (descripcion_especialidad) VALUES ('Astronomy');
INSERT INTO especialidad (descripcion_especialidad) VALUES ('Public Speaking');

INSERT INTO categoria (descripcion_categoria) VALUES ('Coordinador');
INSERT INTO categoria (descripcion_categoria) VALUES ('Speaker');
INSERT INTO categoria (descripcion_categoria) VALUES ('Expert');

INSERT INTO vehiculo (patente, ano_vehiculo, marca, capacidad) VALUES ('as45fe', 2017, 'Chevy', 4);
INSERT INTO vehiculo (patente, ano_vehiculo, marca, capacidad) VALUES ('lj58bv', 2002, 'Mazda', 20);
INSERT INTO vehiculo (patente, ano_vehiculo, marca, capacidad) VALUES ('49fd38', 2001, 'Subaru', 10);
INSERT INTO vehiculo (patente, ano_vehiculo, marca, capacidad) VALUES ('tasdf8', 2001, 'Toyota', 10);

INSERT INTO region (nombre_region) VALUES ('Florida');
INSERT INTO region (nombre_region) VALUES ('Alabama');
INSERT INTO region (nombre_region) VALUES ('Arizona');

INSERT INTO ciudad (nombre_ciudad, id_region) VALUES ('Pensacola', 1);
INSERT INTO ciudad (nombre_ciudad, id_region) VALUES ('Montgomery', 2);
INSERT INTO ciudad (nombre_ciudad, id_region) VALUES ('Pheonix', 3);

INSERT INTO guia (rut_guia, nombre_guia, calle, numero, id_ciudad) VALUES ('266304578', 'John Smith', 'Edison Dr.', 214, 1);
INSERT INTO guia (rut_guia, nombre_guia, calle, numero, id_ciudad) VALUES ('26640457k', 'Jack Offer', 'Jackson Ave.', 1023, 2);
INSERT INTO guia (rut_guia, nombre_guia, calle, numero, id_ciudad) VALUES ('26644457k', 'Mike Toga', 'Madison Way', 342, 3);

INSERT INTO tour (nombre_tour, costo_indiv, costo_grupal, minima_personas, id_ciudad) VALUES ('Los Rios', 200000, 150000, 1, 1);
INSERT INTO tour (nombre_tour, costo_indiv, costo_grupal, minima_personas, id_ciudad) VALUES ('Valle de la Luna', 300000, 120000, 10, 2);
INSERT INTO tour (nombre_tour, costo_indiv, costo_grupal, minima_personas, id_ciudad) VALUES ('Peru', 400000, 110000, 20, 3);
INSERT INTO tour (nombre_tour, costo_indiv, costo_grupal, minima_personas, id_ciudad) VALUES ('San Pedro', 400000, 110000, 20, 3);

INSERT INTO lugar (nombre_lugar, valor_entrada, nivel, id_ciudad) VALUES ('El Bio Bio', 0, 3, 1);
INSERT INTO lugar (nombre_lugar, valor_entrada, nivel, id_ciudad) VALUES ('Lago San Pedro', 10000, 3, 2);
INSERT INTO lugar (nombre_lugar, valor_entrada, nivel, id_ciudad) VALUES ('Atacama', 30000, 0, 3);
INSERT INTO lugar (nombre_lugar, valor_entrada, nivel, id_ciudad) VALUES ('Kilamanjaro', 30000, 5, 3);

INSERT INTO turista (rut_turista, nombre_turista, fecha_nacimento, id_contacto) VALUES ('226634572', 'Jack Asserton', '1985-02-23', 1);
INSERT INTO turista (rut_turista, nombre_turista, fecha_nacimento, id_contacto) VALUES ('194333573', 'Tommy Gunner', '1990-06-02', 2);
INSERT INTO turista (rut_turista, nombre_turista, fecha_nacimento, id_contacto) VALUES ('17398380k', 'Jimmy Johnson', '1955-01-01', 3);
INSERT INTO turista (rut_turista, nombre_turista, fecha_nacimento, id_contacto) VALUES ('232322226', 'Jimmy Olson', '1985-04-03', 1);

INSERT INTO tiene_enfermedad (rut_turista, id_enfermedad) VALUES ('226634572', 1);
INSERT INTO tiene_enfermedad (rut_turista, id_enfermedad) VALUES ('194333573', 2);
INSERT INTO tiene_enfermedad (rut_turista, id_enfermedad) VALUES ('17398380k', 3);

INSERT INTO descuento (descripcion_descuento, porcentaje) VALUES ('0-15 años', 0.20)
INSERT INTO descuento (descripcion_descuento, porcentaje) VALUES ('16-50 años', 0.00)
INSERT INTO descuento (descripcion_descuento, porcentaje) VALUES ('51', 0.10)
INSERT INTO descuento (descripcion_descuento, porcentaje) VALUES ('66 en adelante', 0.30)

INSERT INTO realiza (id_tour, rut_turista, id_descuento) VALUES (2, '232322226', 2);
INSERT INTO realiza (id_tour, rut_turista, id_descuento) VALUES (3, '17398380k', 2);
INSERT INTO realiza (id_tour, rut_turista, id_descuento) VALUES (3, '194333573', 2);
INSERT INTO realiza (id_tour, rut_turista, id_descuento) VALUES (1, '226634572', 2);
INSERT INTO realiza (id_tour, rut_turista, id_descuento) VALUES (4, '17398380k', 1);
INSERT INTO realiza (id_tour, rut_turista, id_descuento) VALUES (2, '194333573', 2);
INSERT INTO realiza (id_tour, rut_turista, id_descuento) VALUES (4, '194333573', 1);
INSERT INTO realiza (id_tour, rut_turista, id_descuento) VALUES (4, '226634572', 3);

INSERT INTO arrienda (patente, rut_empresa, precio, fecha_devolucion) VALUES ('as45fe', '566034578', 120000, '2018-11-29');
INSERT INTO arrienda (patente, rut_empresa, precio, fecha_devolucion) VALUES ('lj58bv', '56404537k', 100000, '2018-11-29');
INSERT INTO arrienda (patente, rut_empresa, precio, fecha_devolucion) VALUES ('49fd38', '566003458', 150000, '2018-11-29');

INSERT INTO posee (rut_guia, id_especialidad, nivel_especialidad) VALUES ('266304578', 1, 0);
INSERT INTO posee (rut_guia, id_especialidad, nivel_especialidad) VALUES ('26640457k', 2, 3);
INSERT INTO posee (rut_guia, id_especialidad, nivel_especialidad) VALUES ('26644457k', 3, 5);
INSERT INTO posee (rut_guia, id_especialidad, nivel_especialidad) VALUES ('266304578', 2, 5);
INSERT INTO posee (rut_guia, id_especialidad, nivel_especialidad) VALUES ('266304578', 3, 5);
INSERT INTO posee (rut_guia, id_especialidad, nivel_especialidad) VALUES ('266304578', 4, 5);
INSERT INTO posee (rut_guia, id_especialidad, nivel_especialidad) VALUES ('26640457k', 1, 5);

INSERT INTO participa (id_tour, rut_guia, id_categoria) VALUES (1, '26640457k', 1);
INSERT INTO participa (id_tour, rut_guia, id_categoria) VALUES (2, '266304578', 1);
INSERT INTO participa (id_tour, rut_guia, id_categoria) VALUES (2, '26640457k', 1);
INSERT INTO participa (id_tour, rut_guia, id_categoria) VALUES (3, '26644457k', 1);

INSERT INTO asociado (id_tour, id_lugar, fecha_llegada, hora_llegada, fecha_salida, hora_salida) VALUES (1, 1, '2018-01-10', '04:15', '2018-01-10', '05:15');
INSERT INTO asociado (id_tour, id_lugar, fecha_llegada, hora_llegada, fecha_salida, hora_salida) VALUES (2, 2, '2018-02-22', '09:35', '2018-02-22', '10:35');
INSERT INTO asociado (id_tour, id_lugar, fecha_llegada, hora_llegada, fecha_salida, hora_salida) VALUES (3, 3, '2018-03-30', '16:40', '2018-03-30', '19:10');
INSERT INTO asociado (id_tour, id_lugar, fecha_llegada, hora_llegada, fecha_salida, hora_salida) VALUES (4, 3, '2018-10-15', '12:40', '2018-10-15', '13:10');
INSERT INTO asociado (id_tour, id_lugar, fecha_llegada, hora_llegada, fecha_salida, hora_salida) VALUES (2, 4, '2018-10-15', '12:40', '2018-10-15', '13:10');

INSERT INTO requerir_auto (id_tour, patente, chofer) VALUES (1, 'as45fe', 'Luke Hobbs');
INSERT INTO requerir_auto (id_tour, patente, chofer) VALUES (1, 'lj58bv', 'Dominic Toretto');
INSERT INTO requerir_auto (id_tour, patente, chofer) VALUES (1, '49fd38', 'Roman Pearce');
