CREATE VIEW VALORES_TOURS(idT, nombreT, TotalVentas)
AS (
  SELECT T.id_tour, T.nombre_tour,
    ((CASE
      WHEN T.minima_personas <= (SELECT COUNT(rut_turista) FROM realiza WHERE (id_tour = T.id_tour))
        THEN (SELECT costo_grupal FROM tour WHERE (id_tour = T.id_tour))
      ELSE (SELECT costo_indiv FROM tour WHERE (id_tour = T.id_tour))
    END) * COUNT(rut_turista)) AS cantidad
  FROM tour T
  JOIN realiza R ON (T.id_tour = R.id_tour)
  GROUP BY (T.id_tour)
);

CREATE VIEW TOUR_DESCUENTOS(idT, nombreT, TotalDescuentos)
AS (
  SELECT T.id_tour, T.nombre_tour,
    SUM(DISTINCT (SELECT porcentaje FROM descuento WHERE (id_descuento = R.id_descuento)) *
    (SELECT COUNT(rut_turista) FROM realiza WHERE (id_tour = R.id_tour AND id_descuento = R.id_descuento)) *
    (CASE
      WHEN T.minima_personas <= (SELECT COUNT(rut_turista) FROM realiza WHERE (id_tour = T.id_tour))
        THEN (SELECT costo_grupal FROM tour WHERE (id_tour = T.id_tour))
      ELSE (SELECT costo_indiv FROM tour WHERE (id_tour = T.id_tour))
    END)) AS test
  FROM tour T
  JOIN realiza R ON (T.id_tour = R.id_tour)
  WHERE (R.id_descuento != 2)
  GROUP BY (T.id_tour)
);

SELECT V.nombreT, (V.TotalVentas - COALESCE(MAX(T.TotalDescuentos), 0)) AS ValorTotalRecibido
FROM VALORES_TOURS V
FULL JOIN TOUR_DESCUENTOS T ON (T.idT = V.idT)
GROUP BY (V.nombreT, V.TotalVentas);
