using LibSernatur.Misc;

int main (string[] args) {
    Test.init (ref args);
    if (args.length > 1) {
		for (int i = 1; i < args.length; i++) {
			switch (args[i]) {
				case "money":
					add_money_tests ();
					break;
				case "rut":
					add_rut_tests ();
					break;
				case "percentage":
					add_percentage_tests ();
					break;
				default:
					error ("Invalid test specified!");
			}
		}
	}
	else {
		add_money_tests ();
		add_rut_tests ();
		add_percentage_tests ();
	}
    return Test.run ();
}

void add_money_tests () {
    Test.add_func ("/sernatur/test/money", () => {
		assert (Money.format_int (100000) == "$100.000");
		assert (Money.format_uint (100000) == "$100.000");
		assert (Money.format_string ("100000") == "$100.000");
    });
}

void add_percentage_tests () {
    Test.add_func ("/sernatur/test/percentage", () => {
		assert (Percentage.format_float ((float) 0.07) == "7%");
		assert (Percentage.format_float ((float) 0.43) == "43%");
		assert (Percentage.format_float ((float) 1) == "100%");
		assert (Percentage.format_double (0.07) == "7%");
		assert (Percentage.format_double (0.43) == "43%");
		assert (Percentage.format_double (1) == "100%");
    });
}

void add_rut_tests () {
    Test.add_func ("/sernatur/test/rut", () => {
		try {
			Rut rut = new Rut ("18166316-2");
			assert (rut.get_clean_rut () == "181663162");
			assert (rut.get_rut () == "18.166.316-2");
			assert (rut.type () == Rut.Type.RUN);

			rut = new Rut ("181663162");
			assert (rut.get_clean_rut () == "181663162");
			assert (rut.get_rut () == "18.166.316-2");
			assert (rut.type () == Rut.Type.RUN);

			rut = new Rut ("20.380.508-k");
			assert (rut.get_clean_rut () == "20380508K");
			assert (rut.get_rut () == "20.380.508-K");
			assert (rut.type () == Rut.Type.RUN);

			rut = new Rut ("50380508-1");
			assert (rut.get_clean_rut () == "503805081");
			assert (rut.get_rut () == "50.380.508-1");
			assert (rut.type () == Rut.Type.RUT);
		}
		catch (Error e) {
			error (e.message);
		}
    });
}
