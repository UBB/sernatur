# SERNATUR
Este proyecto es para conectar a un base de datos de Postgresql para el ramo de Base de Datos.

## Profesora
Valeria Beratto

## Requisitos
- Vala
- Meson
- Ninja
- Gtk+-3
- Libpq
- Valadoc (opcional para compilar valadoc del proyecto)

## Compilar
    meson --prefix=/usr build
    cd build
    ninja

## Opciones de compilación
	-D valadocs=true
Esta opción va a compilar la documentación de codiǵo fuente usando valadoc

	-D valadocs_dep=true
Si ambos valadocs y esta opción están en uso se va a compilar la documentación del proyecto y tambien la documentación de todos los dependencias. Esta opción es caro y hace mucho mas lento la compilación.

	-D debugging=true
Compilar con flags de depuración. Eso implica errores fatales que generan core dumps para ayudar con la depuración.

	-D install_translations¿true
Si debe instalar los traducciones a otras idiomas ó no.