/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace Sernatur {
	using LibSernatur.Misc;
	using LibSernatur.DB;

	/**
	 * The tour list window class
	 */
	[GtkTemplate (ui = "/cl/cromer/ubb/sernatur/tour.list.ui")]
	public class TourList : Gtk.ApplicationWindow {
		/**
		 * The open database connection
		 */
		private Connection conn;
		/**
		 * The columns of the tree view
		 */
		private enum Column {
			/**
			 * The tour name
			 */
			TOUR_NAME,
			/**
			 * The individual cost
			 */
			INDIV_COST,
			/**
			 * The group cost
			 */
			GROUP_COST,
			/**
			 * The minimum people
			 */
			MINIMUM_PEOPLE,
			/**
			 * The name of the city
			 */
			CITY,
			/**
			 * The name of the region
			 */
			REGION,
			/**
			 * The tour object
			 */
			TOUR,
			/**
			 * The number of colums in this enum
			 */
			N_COLUMNS
		}
		/**
		 * The list that stores the contents in the tree view
		 */
		private Gtk.ListStore list_store;
		/**
		 * The list of tours
		 */
		private List<Tour> tour_list;
		/**
		 * The tree view widget
		 */
		[GtkChild]
		private Gtk.TreeView tour_tree;
		/**
		 * Thew new tour button
		 */
		[GtkChild]
		private Gtk.Button new_tour;
		/**
		 * The edit tour button
		 */
		[GtkChild]
		private Gtk.Button edit_tour;
		/**
		 * The delete tour button
		 */
		[GtkChild]
		private Gtk.Button delete_tour;
		/**
		 * The close tour button
		 */
		[GtkChild]
		private Gtk.Button close_tour;
		/**
		 * The tour name column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn tour_name;
		/**
		 * The individual cost column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn indiv_cost;
		/**
		 * The group cost column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn group_cost;
		/**
		 * The minimum number of people column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn minimum_people;
		/**
		 * The city column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn city;
		/**
		 * The region column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn region;
		/**
		 * The row selection
		 */
		[GtkChild]
		private Gtk.TreeSelection selection;

		/**
		 * This callback is called when the user clicks on a row
		 * @param selection The selection object
		 */
		[GtkCallback]
		private void on_changed_selection(Gtk.TreeSelection selection) {
			if (selection.count_selected_rows () == 1) {
				edit_tour.sensitive = true;
				delete_tour.sensitive =true;
			}
			else if (selection.count_selected_rows () > 1) {
				edit_tour.sensitive = false;
				delete_tour.sensitive = true;
			}
			else {
				edit_tour.sensitive = false;
				delete_tour.sensitive = false;
			}
		}

		/**
		 * This callback is run when the user clicks on a button
		 * @param button The button that was clicked
		 */
		[GtkCallback]
		private void on_clicked_button (Gtk.Button button) {
			if (button == new_tour) {
				var tour_editor = new TourEditor (application, conn, null);
				tour_editor.set_transient_for (this); // Set this window as the parent of the new window
				tour_editor.initialize ();
				tour_editor.show_all ();
				tour_editor.save_tour.connect (on_save);
			}
			else if (button == edit_tour) {
				Gtk.TreeModel model;
				var path = selection.get_selected_rows (out model);
				path.foreach ((entry) => {
					var tree_row_reference = new Gtk.TreeRowReference (model, entry);
					Gtk.TreeIter iter;
					list_store.get_iter (out iter, tree_row_reference.get_path ());
					Tour tour;
					model.get (iter,
						Column.TOUR, out tour);
					var tour_editor = new TourEditor (application, conn, tour);
					tour_editor.set_transient_for (this); // Set this window as the parent of the new window
					tour_editor.initialize ();
					tour_editor.show_all ();
					tour_editor.save_tour.connect (on_save);
				});
			}
			else if (button == delete_tour) {
				Gtk.MessageDialog msg;
				if (selection.count_selected_rows () == 1) {
					msg = new Gtk.MessageDialog (this,
						Gtk.DialogFlags.MODAL,
						Gtk.MessageType.ERROR,
						Gtk.ButtonsType.YES_NO,
						_ ("Are you sure you wish to delete this tour?"));
				}
				else {
					msg = new Gtk.MessageDialog (this,
						Gtk.DialogFlags.MODAL,
						Gtk.MessageType.ERROR,
						Gtk.ButtonsType.YES_NO,
						_ ("Are you sure you wish to delete these tours?"));
				}
				msg.response.connect ((response_id) => {
					switch (response_id) {
						case Gtk.ResponseType.YES:
							Gtk.TreeModel model;
							var path = selection.get_selected_rows (out model);
							path.foreach ((entry) => {
								var tree_row_reference = new Gtk.TreeRowReference (model, entry);
								Gtk.TreeIter iter;
								list_store.get_iter (out iter, tree_row_reference.get_path ());
								Tour tour;
								model.get (iter,
									Column.TOUR, out tour);
								try {
									Tour.delete_tour (conn, tour);
								}
								catch (PostgresError e) {
									#if DEBUG
										error (e.message);
									#else
										warning (e.message);
									#endif
								}
								catch (DBError e) {
									if (e.code == 1) {
										warning (e.message);
										var msg2 = new Gtk.MessageDialog (this,
											Gtk.DialogFlags.MODAL,
											Gtk.MessageType.ERROR,
											Gtk.ButtonsType.CLOSE,
											_ ("Error: Could not delete tour \"%s\" because either this tour has been taken or is still associated with a place or vehicle!"), tour.nombre_tour);
										msg2.response.connect ((response_id) => {
											msg2.destroy ();
										});
										msg2.set_title (_ ("Error"));
										msg2.run ();
									}
									else {
										#if DEBUG
											error (e.message);
										#else
											warning (e.message);
										#endif
									}
								}
							});
							edit_tour.sensitive = false;
							delete_tour.sensitive = false;
							reset_columns ();

							list_store.clear ();

							update_list_store ();
							break;
					}
					msg.destroy ();
				});
				msg.show ();
			}
			else if (button == close_tour) {
				this.close ();
			}
		}

		/**
		 * Called when a new or old tour is saved
		 * @param tour_editor The editor that saved the tour
		 */
		public void on_save(TourEditor tour_editor) {
			edit_tour.sensitive = false;
			delete_tour.sensitive = false;
			reset_columns ();

			list_store.clear ();

			update_list_store ();
		}

		/**
		 * This callback is run when the user clicks on a column to reorder the rows
		 * @param column The column that was clicked
		 */
		[GtkCallback]
		private void on_clicked_column (Gtk.TreeViewColumn column) {
			edit_tour.sensitive = false;
			delete_tour.sensitive = false;
			if (column == tour_name) {
				if (!tour_name.sort_indicator) {
					reset_columns ();
					tour_name.sort_indicator = true;
				}

				if (tour_name.sort_order == Gtk.SortType.ASCENDING) {
					tour_name.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					tour_name.sort_order = Gtk.SortType.ASCENDING;
				}

				tour_list.sort_with_data ((a, b) => {
					if (tour_name.sort_order == Gtk.SortType.ASCENDING) {
						return strcmp (a.nombre_tour, b.nombre_tour);
					}
					else {
						return strcmp (b.nombre_tour, a.nombre_tour);
					}
				});
			}
			else if (column == indiv_cost) {
				if (!indiv_cost.sort_indicator) {
					reset_columns ();
					indiv_cost.sort_indicator = true;
				}

				if (indiv_cost.sort_order == Gtk.SortType.ASCENDING) {
					indiv_cost.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					indiv_cost.sort_order = Gtk.SortType.ASCENDING;
				}

				tour_list.sort_with_data ((a, b) => {
					if (indiv_cost.sort_order == Gtk.SortType.ASCENDING) {
						if (a.costo_indiv < b.costo_indiv) {
							return -1;
						}
						else if (a.costo_indiv == b.costo_indiv) {
							return 0;
						}
						else {
							return 1;
						}
					}
					else {
						if (a.costo_indiv < b.costo_indiv) {
							return 1;
						}
						else if (a.costo_indiv == b.costo_indiv) {
							return 0;
						}
						else {
							return -1;
						}
					}
				});
			}
			else if (column == group_cost) {
				if (!group_cost.sort_indicator) {
					reset_columns ();
					group_cost.sort_indicator = true;
				}

				if (group_cost.sort_order == Gtk.SortType.ASCENDING) {
					group_cost.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					group_cost.sort_order = Gtk.SortType.ASCENDING;
				}

				tour_list.sort_with_data ((a, b) => {
					if (group_cost.sort_order == Gtk.SortType.ASCENDING) {
						if (a.costo_grupal < b.costo_grupal) {
							return -1;
						}
						else if (a.costo_grupal == b.costo_grupal) {
							return 0;
						}
						else {
							return 1;
						}
					}
					else {
						if (a.costo_grupal < b.costo_grupal) {
							return 1;
						}
						else if (a.costo_grupal == b.costo_grupal) {
							return 0;
						}
						else {
							return -1;
						}
					}
				});
			}
			else if (column == minimum_people) {
				if (!minimum_people.sort_indicator) {
					reset_columns ();
					minimum_people.sort_indicator = true;
				}

				if (minimum_people.sort_order == Gtk.SortType.ASCENDING) {
					minimum_people.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					minimum_people.sort_order = Gtk.SortType.ASCENDING;
				}

				tour_list.sort_with_data ((a, b) => {
					if (minimum_people.sort_order == Gtk.SortType.ASCENDING) {
						if (a.minima_personas < b.minima_personas) {
							return -1;
						}
						else if (a.minima_personas == b.minima_personas) {
							return 0;
						}
						else {
							return 1;
						}
					}
					else {
						if (a.minima_personas < b.minima_personas) {
							return 1;
						}
						else if (a.minima_personas == b.minima_personas) {
							return 0;
						}
						else {
							return -1;
						}
					}
				});
			}
			else if (column == city) {
				if (!city.sort_indicator) {
					reset_columns ();
					city.sort_indicator = true;
				}

				if (city.sort_order == Gtk.SortType.ASCENDING) {
					city.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					city.sort_order = Gtk.SortType.ASCENDING;
				}

				tour_list.sort_with_data ((a, b) => {
					if (city.sort_order == Gtk.SortType.ASCENDING) {
						return strcmp (a.ciudad.nombre_ciudad, b.ciudad.nombre_ciudad);
					}
					else {
						return strcmp (b.ciudad.nombre_ciudad, a.ciudad.nombre_ciudad);
					}
				});
			}
			else if (column == region) {
				if (!region.sort_indicator) {
					reset_columns ();
					region.sort_indicator = true;
				}

				if (region.sort_order == Gtk.SortType.ASCENDING) {
					region.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					region.sort_order = Gtk.SortType.ASCENDING;
				}

				tour_list.sort_with_data ((a, b) => {
					if (region.sort_order == Gtk.SortType.ASCENDING) {
						return strcmp (a.ciudad.region.nombre_region, b.ciudad.region.nombre_region);
					}
					else {
						return strcmp (b.ciudad.region.nombre_region, a.ciudad.region.nombre_region);
					}
				});
			}

			list_store.clear ();
			tour_list.foreach ((entry) => {
				Gtk.TreeIter iter;
				list_store.append (out iter);
				list_store.set (iter,
					Column.TOUR_NAME, entry.nombre_tour,
					Column.INDIV_COST, Money.format_uint (entry.costo_indiv),
					Column.GROUP_COST, Money.format_uint (entry.costo_grupal),
					Column.MINIMUM_PEOPLE, entry.minima_personas,
					Column.CITY, entry.ciudad.nombre_ciudad,
					Column.REGION, entry.ciudad.region.nombre_region,
					Column.TOUR, entry);
			});
		}

		/**
		 * Reset the sort indicator and order of all the columns
		 */
		private void reset_columns () {
			tour_name.sort_indicator = false;
			tour_name.sort_order = Gtk.SortType.DESCENDING;
			indiv_cost.sort_indicator = false;
			indiv_cost.sort_order = Gtk.SortType.DESCENDING;
			group_cost.sort_indicator = false;
			group_cost.sort_order = Gtk.SortType.DESCENDING;
			minimum_people.sort_indicator = false;
			minimum_people.sort_order = Gtk.SortType.DESCENDING;
			city.sort_indicator = false;
			city.sort_order = Gtk.SortType.DESCENDING;
			region.sort_indicator = false;
			region.sort_order = Gtk.SortType.DESCENDING;
		}

		/**
		 * Update the list store with the data from the database
		 */
		private void update_list_store () {
			try {
				tour_list = Tour.get_all_tours (conn);
			}
			catch (Error e) {
				#if DEBUG
					error (e.message);
				#else
					warning (e.message);
				#endif
			}

			tour_list.foreach ((entry) => {
				Gtk.TreeIter iter;
				list_store.append (out iter);
				list_store.set (iter,
					Column.TOUR_NAME, entry.nombre_tour,
					Column.INDIV_COST, Money.format_uint (entry.costo_indiv),
					Column.GROUP_COST, Money.format_uint (entry.costo_grupal),
					Column.MINIMUM_PEOPLE, entry.minima_personas,
					Column.CITY, entry.ciudad.nombre_ciudad,
					Column.REGION, entry.ciudad.region.nombre_region,
					Column.TOUR, entry);
			});
		}

		/**
		 * Initialize the tour list class
		 * @param application The application used to make the GLib object
		 * @param conn The database connection to use
		 */
		public TourList (Gtk.Application application, Connection conn) {
			Object (application: application);
			this.conn = conn;

			this.set_visible (true); // This fixes: Gtk-CRITICAL **: 23:58:22.139: gtk_box_gadget_distribute: assertion 'size >= 0' failed in GtkScrollbar
		}

		/**
		 * Initialize what is needed for this window
		 */
		public void initialize () {
			list_store = new Gtk.ListStore (Column.N_COLUMNS,
				typeof (string),
				typeof (string),
				typeof (string),
				typeof (uint),
				typeof (string),
				typeof (string),
				typeof (Tour));

			update_list_store ();

			tour_tree.set_model (list_store);
		}
	}
}
