/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace Sernatur {
	using LibSernatur.Misc;
	using LibSernatur.DB;

	/**
	 * The staff list window class
	 */
	[GtkTemplate (ui = "/cl/cromer/ubb/sernatur/staff.list.ui")]
	public class StaffList : Gtk.ApplicationWindow {
		/**
		 * The open database connection
		 */
		private Connection conn;
		/**
		 * The columns of the tree view
		 */
		private enum Column {
			/**
			 * The staff's run
			 */
			RUN,
			/**
			 * The staff's name
			 */
			NAME,
			/**
			 * The staff's street
			 */
			STREET,
			/**
			 * The staff's number
			 */
			NUMBER,
			/**
			 * The name of the city
			 */
			CITY,
			/**
			 * The name of the region
			 */
			REGION,
			/**
			 * The staff object
			 */
			STAFF,
			/**
			 * The number of colums in this enum
			 */
			N_COLUMNS
		}
		/**
		 * The list that stores the contents in the tree view
		 */
		private Gtk.ListStore list_store;
		/**
		 * The list of staffs
		 */
		private List<Guia> staff_list;
		/**
		 * The tree view widget
		 */
		[GtkChild]
		private Gtk.TreeView staff_tree;
		/**
		 * Thew new staff button
		 */
		[GtkChild]
		private Gtk.Button new_staff;
		/**
		 * The edit staff button
		 */
		[GtkChild]
		private Gtk.Button edit_staff;
		/**
		 * The delete staff button
		 */
		[GtkChild]
		private Gtk.Button delete_staff;
		/**
		 * The close staff button
		 */
		[GtkChild]
		private Gtk.Button close_staff;
		/**
		 * The staff's RUN
		 */
		[GtkChild]
		private Gtk.TreeViewColumn run;
		/**
		 * The staff's name
		 */
		[GtkChild]
		private Gtk.TreeViewColumn guide_name;
		/**
		 * The street
		 */
		[GtkChild]
		private Gtk.TreeViewColumn street;
		/**
		 * The number
		 */
		[GtkChild]
		private Gtk.TreeViewColumn number;
		/**
		 * The row selection
		 */
		[GtkChild]
		private Gtk.TreeSelection selection;
		/**
		 * The city column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn city;
		/**
		 * The region column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn region;
		/**
		 * This callback is called when the user clicks on a row
		 * @param selection The selection object
		 */
		[GtkCallback]
		private void on_changed_selection(Gtk.TreeSelection selection) {
			if (selection.count_selected_rows () == 1) {
				edit_staff.sensitive = true;
				delete_staff.sensitive =true;
			}
			else if (selection.count_selected_rows () > 1) {
				edit_staff.sensitive = false;
				delete_staff.sensitive = true;
			}
			else {
				edit_staff.sensitive = false;
				delete_staff.sensitive = false;
			}
		}

		/**
		 * This callback is run when the user clicks on a button
		 * @param button The button that was clicked
		 */
		[GtkCallback]
		private void on_clicked_button (Gtk.Button button) {
			if (button == new_staff) {
				var staff_editor = new StaffEditor (application, conn, null);
				staff_editor.set_transient_for (this); // Set this window as the parent of the new window
				staff_editor.initialize ();
				staff_editor.show_all ();
				staff_editor.save_staff.connect (on_save);
			}
			else if (button == edit_staff) {
				Gtk.TreeModel model;
				var path = selection.get_selected_rows (out model);
				path.foreach ((entry) => {
					var tree_row_reference = new Gtk.TreeRowReference (model, entry);
					Gtk.TreeIter iter;
					list_store.get_iter (out iter, tree_row_reference.get_path ());
					Guia staff;
					model.get (iter,
						Column.STAFF, out staff);
					var staff_editor = new StaffEditor (application, conn, staff);
					staff_editor.set_transient_for (this); // Set this window as the parent of the new window
					staff_editor.initialize ();
					staff_editor.show_all ();
					staff_editor.save_staff.connect (on_save);
				});
			}
			else if (button == delete_staff) {
				Gtk.MessageDialog msg;
				if (selection.count_selected_rows () == 1) {
					msg = new Gtk.MessageDialog (this,
						Gtk.DialogFlags.MODAL,
						Gtk.MessageType.ERROR,
						Gtk.ButtonsType.YES_NO,
						_ ("Are you sure you wish to delete this staff member?"));
				}
				else {
					msg = new Gtk.MessageDialog (this,
						Gtk.DialogFlags.MODAL,
						Gtk.MessageType.ERROR,
						Gtk.ButtonsType.YES_NO,
						_ ("Are you sure you wish to delete these staff members?"));
				}
				msg.response.connect ((response_id) => {
					switch (response_id) {
						case Gtk.ResponseType.YES:
							Gtk.TreeModel model;
							var path = selection.get_selected_rows (out model);
							path.foreach ((entry) => {
								var tree_row_reference = new Gtk.TreeRowReference (model, entry);
								Gtk.TreeIter iter;
								list_store.get_iter (out iter, tree_row_reference.get_path ());
								Guia staff;
								model.get (iter,
									Column.STAFF, out staff);
								try {
									Guia.delete_staff (conn, staff);
								}
								catch (PostgresError e) {
									#if DEBUG
										error (e.message);
									#else
										warning (e.message);
									#endif
								}
							});
							edit_staff.sensitive = false;
							delete_staff.sensitive = false;
							reset_columns ();

							list_store.clear ();

							update_list_store ();
							break;
					}
					msg.destroy ();
				});
				msg.show ();
			}
			else if (button == close_staff) {
				this.close ();
			}
		}

		/**
		 * Called when a new or old staff is saved
		 * @param staff_editor The editor that saved the staff
		 */
		public void on_save(StaffEditor staff_editor) {
			edit_staff.sensitive = false;
			delete_staff.sensitive = false;
			reset_columns ();

			list_store.clear ();

			update_list_store ();
		}

		/**
		 * This callback is run when the user clicks on a column to reorder the rows
		 * @param column The column that was clicked
		 */
		[GtkCallback]
		private void on_clicked_column (Gtk.TreeViewColumn column) {
			edit_staff.sensitive = false;
			delete_staff.sensitive = false;
			if (column == run) {
				if (!run.sort_indicator) {
					reset_columns ();
					run.sort_indicator = true;
				}

				if (run.sort_order == Gtk.SortType.ASCENDING) {
					run.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					run.sort_order = Gtk.SortType.ASCENDING;
				}

				staff_list.sort_with_data ((a, b) => {
					if (run.sort_order == Gtk.SortType.ASCENDING) {
						return strcmp (a.rut_guia, b.rut_guia);
					}
					else {
						return strcmp (b.rut_guia, a.rut_guia);
					}
				});
			}
			else if (column == guide_name) {
				if (!guide_name.sort_indicator) {
					reset_columns ();
					guide_name.sort_indicator = true;
				}

				if (guide_name.sort_order == Gtk.SortType.ASCENDING) {
					guide_name.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					guide_name.sort_order = Gtk.SortType.ASCENDING;
				}

				staff_list.sort_with_data ((a, b) => {
					if (guide_name.sort_order == Gtk.SortType.ASCENDING) {
						return strcmp (a.nombre_guia, b.nombre_guia);
					}
					else {
						return strcmp (b.nombre_guia, a.nombre_guia);
					}
				});
			}
			else if (column == street) {
				if (!street.sort_indicator) {
					reset_columns ();
					street.sort_indicator = true;
				}

				if (street.sort_order == Gtk.SortType.ASCENDING) {
					street.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					street.sort_order = Gtk.SortType.ASCENDING;
				}

				staff_list.sort_with_data ((a, b) => {
					if (street.sort_order == Gtk.SortType.ASCENDING) {
						return strcmp (a.calle, b.calle);
					}
					else {
						return strcmp (b.calle, a.calle);
					}
				});
			}
			else if (column == number) {
				if (!number.sort_indicator) {
					reset_columns ();
					number.sort_indicator = true;
				}

				if (number.sort_order == Gtk.SortType.ASCENDING) {
					number.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					number.sort_order = Gtk.SortType.ASCENDING;
				}

				staff_list.sort_with_data ((a, b) => {
					if (number.sort_order == Gtk.SortType.ASCENDING) {
						if (a.numero < b.numero) {
							return -1;
						}
						else if (a.numero == b.numero) {
							return 0;
						}
						else {
							return 1;
						}
					}
					else {
						if (a.numero < b.numero) {
							return 1;
						}
						else if (a.numero == b.numero) {
							return 0;
						}
						else {
							return -1;
						}
					}
				});
			}
			else if (column == city) {
				if (!city.sort_indicator) {
					reset_columns ();
					city.sort_indicator = true;
				}

				if (city.sort_order == Gtk.SortType.ASCENDING) {
					city.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					city.sort_order = Gtk.SortType.ASCENDING;
				}

				staff_list.sort_with_data ((a, b) => {
					if (city.sort_order == Gtk.SortType.ASCENDING) {
						return strcmp (a.ciudad.nombre_ciudad, b.ciudad.nombre_ciudad);
					}
					else {
						return strcmp (b.ciudad.nombre_ciudad, a.ciudad.nombre_ciudad);
					}
				});
			}
			else if (column == region) {
				if (!region.sort_indicator) {
					reset_columns ();
					region.sort_indicator = true;
				}

				if (region.sort_order == Gtk.SortType.ASCENDING) {
					region.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					region.sort_order = Gtk.SortType.ASCENDING;
				}

				staff_list.sort_with_data ((a, b) => {
					if (region.sort_order == Gtk.SortType.ASCENDING) {
						return strcmp (a.ciudad.region.nombre_region, b.ciudad.region.nombre_region);
					}
					else {
						return strcmp (b.ciudad.region.nombre_region, a.ciudad.region.nombre_region);
					}
				});
			}

			list_store.clear ();
			staff_list.foreach ((entry) => {
				Gtk.TreeIter iter;
				list_store.append (out iter);
				try {
					list_store.set (iter,
						Column.RUN, new Rut (entry.rut_guia).get_rut (),
						Column.NAME, entry.nombre_guia,
						Column.STREET, entry.calle,
						Column.NUMBER, entry.numero,
						Column.CITY, entry.ciudad.nombre_ciudad,
						Column.REGION, entry.ciudad.region.nombre_region,
						Column.STAFF, entry);
				}
				catch (Error e) {
					#if DEBUG
						error (e.message);
					#else
						warning (e.message);
					#endif
				}
			});
		}

		/**
		 * Reset the sort indicator and order of all the columns
		 */
		private void reset_columns () {
			run.sort_indicator = false;
			run.sort_order = Gtk.SortType.DESCENDING;
			guide_name.sort_indicator = false;
			guide_name.sort_order = Gtk.SortType.DESCENDING;
			street.sort_indicator = false;
			street.sort_order = Gtk.SortType.DESCENDING;
			number.sort_indicator = false;
			number.sort_order = Gtk.SortType.DESCENDING;
			city.sort_indicator = false;
			city.sort_order = Gtk.SortType.DESCENDING;
			region.sort_indicator = false;
			region.sort_order = Gtk.SortType.DESCENDING;
		}

		/**
		 * Update the list store with the data from the database
		 */
		private void update_list_store () {
			try {
				staff_list = Guia.get_all_guias (conn);
			}
			catch (Error e) {
				#if DEBUG
					error (e.message);
				#else
					warning (e.message);
				#endif
			}

			staff_list.foreach ((entry) => {
				Gtk.TreeIter iter;
				list_store.append (out iter);
				try {
					list_store.set (iter,
						Column.RUN, new Rut (entry.rut_guia).get_rut (),
						Column.NAME, entry.nombre_guia,
						Column.STREET, entry.calle,
						Column.NUMBER, entry.numero,
						Column.CITY, entry.ciudad.nombre_ciudad,
						Column.REGION, entry.ciudad.region.nombre_region,
						Column.STAFF, entry);
				}
				catch (Error e) {
					#if DEBUG
						error (e.message);
					#else
						warning (e.message);
					#endif
				}
			});
		}

		/**
		 * Initialize the staff list class
		 * @param application The application used to make the GLib object
		 * @param conn The database connection to use
		 */
		public StaffList (Gtk.Application application, Connection conn) {
			Object (application: application);
			this.conn = conn;

			this.set_visible (true); // This fixes: Gtk-CRITICAL **: 23:58:22.139: gtk_box_gadget_distribute: assertion 'size >= 0' failed in GtkScrollbar
		}

		/**
		 * Initialize what is needed for this window
		 */
		public void initialize () {
			list_store = new Gtk.ListStore (Column.N_COLUMNS,
				typeof (string),
				typeof (string),
				typeof (string),
				typeof (uint),
				typeof (string),
				typeof (string),
				typeof (Guia));

			update_list_store ();

			staff_tree.set_model (list_store);
		}
	}
}
