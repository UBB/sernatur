/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace Sernatur {
	using LibSernatur.Misc;
	using LibSernatur.DB;

	/**
	 * The tour place window class
	 */
	[GtkTemplate (ui = "/cl/cromer/ubb/sernatur/tour.places.ui")]
	public class TourPlaces : Gtk.ApplicationWindow {
		/**
		 * The open database connection
		 */
		private Connection conn;
		/**
		 * The tour to edit
		 */
		private Tour tour;
		/**
		 * The columns of the tree view
		 */
		private enum Column {
			/**
			 * The name of the place
			 */
			PLACE_NAME,
			/**
			 * The individual cost
			 */
			TICKET_PRICE,
			/**
			 * The level of difficulty
			 */
			DIFFICULTY,
			/**
			 * The date of arrival
			 */
			ARRIVAL_DATE,
			/**
			 * The time of arrival
			 */
			ARRIVAL_TIME,
			/**
			 * The date of departure
			 */
			DEPARTURE_DATE,
			/**
			 * The departure time
			 */
			DEPARTURE_TIME,
			/**
			 * The associated object
			 */
			ASSOCIATED,
			/**
			 * The number of colums in this enum
			 */
			N_COLUMNS
		}
		/**
		 * The list that stores the contents in the tree view
		 */
		private Gtk.ListStore list_store;
		/**
		 * The list of associations
		 */
		private List<Asociado> associated_list;
		/**
		 * The tree view widget
		 */
		[GtkChild]
		private Gtk.TreeView place_tree;
		/**
		 * The place name column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn place_name;
		/**
		 * The ticket price column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn ticket_price;
		/**
		 * The difficulty column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn difficulty;
		/**
		 * The arrival date column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn arrival_date;
		/**
		 * The arrival time column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn arrival_time;
		/**
		 * The departure date column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn departure_date;
		/**
		 * The departure time column
		 */
		[GtkChild]
		private Gtk.TreeViewColumn departure_time;
		/**
		 * Thew new place button
		 */
		[GtkChild]
		private Gtk.Button new_place;
		/**
		 * The close place button
		 */
		[GtkChild]
		private Gtk.Button close_place;
		/**
		 * The add place button
		 */
		[GtkChild]
		private Gtk.Button add_place;
		/**
		 * The associated tours to save if the user decides to save the tour
		 */
		private List<Asociado> asociado_list = new List<Asociado> ();
		/**
		 * This signal is called when there are places that need to be saved after the tour is created
		 */
		public signal void save_places (List<Asociado> asociado_list);

		/**
		 * This callback is called when the user clicks on a row
		 * @param selection The selection object
		 */
		[GtkCallback]
		private void on_changed_selection(Gtk.TreeSelection selection) {
			// Not implemented due to lack of time
			/*if (selection.count_selected_rows () == 1) {
				edit_place.sensitive = true;
				delete_place.sensitive =true;
			}
			else if (selection.count_selected_rows () > 1) {
				edit_place.sensitive = false;
				delete_place.sensitive = true;
			}
			else {
				edit_place.sensitive = false;
				delete_place.sensitive = false;
			}*/
		}

		/**
		 * This callback is run when the user clicks on a button
		 * @param button The button that was clicked
		 */
		[GtkCallback]
		private void on_clicked_button (Gtk.Button button) {
			if (button == new_place) {
				var place_editor = new PlaceEditor (application, conn, null);
				place_editor.set_transient_for (this); // Set this window as the parent of the new window
				place_editor.initialize ();
				place_editor.show_all ();
				place_editor.save_place.connect (on_save_place);
			}
			else if (button == add_place) {
				var place_editor = new TourPlaceEditor (application, conn, new Asociado ("", "", "", "", tour, null));
				place_editor.set_transient_for (this); // Set this window as the parent of the new window
				place_editor.initialize ();
				place_editor.show_all ();
				place_editor.save_place.connect (on_save_tour_place);
			}
			/*else if (button == edit_place) {
				Gtk.TreeModel model;
				var path = selection.get_selected_rows (out model);
				path.foreach ((entry) => {
					var tree_row_reference = new Gtk.TreeRowReference (model, entry);
					Gtk.TreeIter iter;
					list_store.get_iter (out iter, tree_row_reference.get_path ());
					Asociado asociado;
					model.get (iter,
						Column.ASSOCIATED, out asociado);
					var place_editor = new TourPlaceEditor (application, conn, asociado);
					place_editor.set_transient_for (this); // Set this window as the parent of the new window
					place_editor.initialize ();
					place_editor.show_all ();
					place_editor.save_place.connect (on_save_tour_place);
				});
			}
			else if (button == delete_place) {
				Gtk.MessageDialog msg;
				if (selection.count_selected_rows () == 1) {
					msg = new Gtk.MessageDialog (this,
						Gtk.DialogFlags.MODAL,
						Gtk.MessageType.ERROR,
						Gtk.ButtonsType.YES_NO,
						_ ("Are you sure you wish to delete this associated place?"));
				}
				else {
					msg = new Gtk.MessageDialog (this,
						Gtk.DialogFlags.MODAL,
						Gtk.MessageType.ERROR,
						Gtk.ButtonsType.YES_NO,
						_ ("Are you sure you wish to delete these associated places?"));
				}
				msg.response.connect ((response_id) => {
					switch (response_id) {
						case Gtk.ResponseType.YES:
							Gtk.TreeModel model;
							var path = selection.get_selected_rows (out model);
							path.foreach ((entry) => {
								var tree_row_reference = new Gtk.TreeRowReference (model, entry);
								Gtk.TreeIter iter;
								list_store.get_iter (out iter, tree_row_reference.get_path ());
								Asociado asociado;
								model.get (iter,
									Column.ASSOCIATED, out asociado);
								try {
									Asociado.delete_associated (conn, asociado);
								}
								catch (PostgresError e) {
									#if DEBUG
										error (e.message);
									#else
										warning (e.message);
									#endif
								}
								catch (DBError e) {
									if (e.code == 1) {
										warning (e.message);
										var msg2 = new Gtk.MessageDialog (this,
											Gtk.DialogFlags.MODAL,
											Gtk.MessageType.ERROR,
											Gtk.ButtonsType.CLOSE,
											_ ("Error: Could not delete the associated place \"%s\"!"), asociado.lugar.nombre_lugar);
										msg2.response.connect ((response_id) => {
											msg2.destroy ();
										});
										msg2.set_title (_ ("Error"));
										msg2.run ();
									}
									else {
										#if DEBUG
											error (e.message);
										#else
											warning (e.message);
										#endif
									}
								}
							});
							edit_place.sensitive = false;
							delete_place.sensitive = false;
							reset_columns ();

							list_store.clear ();

							update_list_store ();
							break;
					}
					msg.destroy ();
				});
				msg.show ();
			}*/
			else if (button == close_place) {
				if (asociado_list.length () > 0) {
					save_places (asociado_list);
				}
				this.close ();
			}
		}

		/**
		 * This callback is run when the user clicks on a column to reorder the rows
		 * @param column The column that was clicked
		 */
		[GtkCallback]
		private void on_clicked_column (Gtk.TreeViewColumn column) {
			/*edit_place.sensitive = false;
			delete_place.sensitive = false;*/
			if (column == place_name) {
				if (!place_name.sort_indicator) {
					reset_columns ();
					place_name.sort_indicator = true;
				}

				if (place_name.sort_order == Gtk.SortType.ASCENDING) {
					place_name.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					place_name.sort_order = Gtk.SortType.ASCENDING;
				}

				associated_list.sort_with_data ((a, b) => {
					if (place_name.sort_order == Gtk.SortType.ASCENDING) {
						return strcmp (a.lugar.nombre_lugar, b.lugar.nombre_lugar);
					}
					else {
						return strcmp (b.lugar.nombre_lugar, a.lugar.nombre_lugar);
					}
				});
			}
			else if (column == ticket_price) {
				if (!ticket_price.sort_indicator) {
					reset_columns ();
					ticket_price.sort_indicator = true;
				}

				if (ticket_price.sort_order == Gtk.SortType.ASCENDING) {
					ticket_price.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					ticket_price.sort_order = Gtk.SortType.ASCENDING;
				}

				associated_list.sort_with_data ((a, b) => {
					if (ticket_price.sort_order == Gtk.SortType.ASCENDING) {
						if (a.lugar.valor_entrada < b.lugar.valor_entrada) {
							return -1;
						}
						else if (a.lugar.valor_entrada == b.lugar.valor_entrada) {
							return 0;
						}
						else {
							return 1;
						}
					}
					else {
						if (a.lugar.valor_entrada < b.lugar.valor_entrada) {
							return 1;
						}
						else if (a.lugar.valor_entrada == b.lugar.valor_entrada) {
							return 0;
						}
						else {
							return -1;
						}
					}
				});
			}
			else if (column == difficulty) {
				if (!difficulty.sort_indicator) {
					reset_columns ();
					difficulty.sort_indicator = true;
				}

				if (difficulty.sort_order == Gtk.SortType.ASCENDING) {
					difficulty.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					difficulty.sort_order = Gtk.SortType.ASCENDING;
				}

				associated_list.sort_with_data ((a, b) => {
					if (difficulty.sort_order == Gtk.SortType.ASCENDING) {
						if (a.lugar.nivel < b.lugar.nivel) {
							return -1;
						}
						else if (a.lugar.nivel == b.lugar.nivel) {
							return 0;
						}
						else {
							return 1;
						}
					}
					else {
						if (a.lugar.nivel < b.lugar.nivel) {
							return 1;
						}
						else if (a.lugar.nivel == b.lugar.nivel) {
							return 0;
						}
						else {
							return -1;
						}
					}
				});
			}
			else if (column == arrival_date) {
				if (!arrival_date.sort_indicator) {
					reset_columns ();
					arrival_date.sort_indicator = true;
				}

				if (arrival_date.sort_order == Gtk.SortType.ASCENDING) {
					arrival_date.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					arrival_date.sort_order = Gtk.SortType.ASCENDING;
				}

				associated_list.sort_with_data ((a, b) => {
					if (arrival_date.sort_order == Gtk.SortType.ASCENDING) {
						return strcmp (a.fecha_llegada, b.fecha_llegada);
					}
					else {
						return strcmp (b.fecha_llegada, a.fecha_llegada);
					}
				});
			}
			else if (column == arrival_time) {
				if (!arrival_time.sort_indicator) {
					reset_columns ();
					arrival_time.sort_indicator = true;
				}

				if (arrival_time.sort_order == Gtk.SortType.ASCENDING) {
					arrival_time.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					arrival_time.sort_order = Gtk.SortType.ASCENDING;
				}

				associated_list.sort_with_data ((a, b) => {
					if (arrival_time.sort_order == Gtk.SortType.ASCENDING) {
						return strcmp (a.hora_llegada, b.hora_llegada);
					}
					else {
						return strcmp (b.hora_llegada, a.hora_llegada);
					}
				});
			}
			else if (column == departure_date) {
				if (!departure_date.sort_indicator) {
					reset_columns ();
					departure_date.sort_indicator = true;
				}

				if (departure_date.sort_order == Gtk.SortType.ASCENDING) {
					departure_date.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					departure_date.sort_order = Gtk.SortType.ASCENDING;
				}

				associated_list.sort_with_data ((a, b) => {
					if (departure_date.sort_order == Gtk.SortType.ASCENDING) {
						return strcmp (a.fecha_salida, b.fecha_salida);
					}
					else {
						return strcmp (b.fecha_salida, a.fecha_salida);
					}
				});
			}
			else if (column == departure_time) {
				if (!departure_time.sort_indicator) {
					reset_columns ();
					departure_time.sort_indicator = true;
				}

				if (departure_time.sort_order == Gtk.SortType.ASCENDING) {
					departure_time.sort_order = Gtk.SortType.DESCENDING;
				}
				else {
					departure_time.sort_order = Gtk.SortType.ASCENDING;
				}

				associated_list.sort_with_data ((a, b) => {
					if (departure_time.sort_order == Gtk.SortType.ASCENDING) {
						return strcmp (a.hora_salida, b.hora_salida);
					}
					else {
						return strcmp (b.hora_salida, a.hora_salida);
					}
				});
			}

			list_store.clear ();
			associated_list.foreach ((entry) => {
				Gtk.TreeIter iter;
				list_store.append (out iter);
				list_store.set (iter,
					Column.PLACE_NAME, entry.lugar.nombre_lugar,
					Column.TICKET_PRICE, Money.format_uint (entry.lugar.valor_entrada),
					Column.DIFFICULTY, entry.lugar.nivel,
					Column.ARRIVAL_DATE, entry.fecha_llegada,
					Column.ARRIVAL_TIME, entry.hora_llegada,
					Column.DEPARTURE_DATE, entry.fecha_salida,
					Column.DEPARTURE_TIME, entry.hora_salida,
					Column.ASSOCIATED, entry);
			});
			asociado_list.foreach ((entry) => {
				Gtk.TreeIter iter;
				list_store.append (out iter);
				list_store.set (iter,
					Column.PLACE_NAME, entry.lugar.nombre_lugar,
					Column.TICKET_PRICE, Money.format_uint (entry.lugar.valor_entrada),
					Column.DIFFICULTY, entry.lugar.nivel,
					Column.ARRIVAL_DATE, entry.fecha_llegada,
					Column.ARRIVAL_TIME, entry.hora_llegada,
					Column.DEPARTURE_DATE, entry.fecha_salida,
					Column.DEPARTURE_TIME, entry.hora_salida,
					Column.ASSOCIATED, entry);
			});
		}

		/**
		 * Reset the sort indicator and order of all the columns
		 */
		private void reset_columns () {
			place_name.sort_indicator = false;
			place_name.sort_order = Gtk.SortType.DESCENDING;
			ticket_price.sort_indicator = false;
			ticket_price.sort_order = Gtk.SortType.DESCENDING;
			difficulty.sort_indicator = false;
			difficulty.sort_order = Gtk.SortType.DESCENDING;
			arrival_date.sort_indicator = false;
			arrival_date.sort_order = Gtk.SortType.DESCENDING;
			arrival_time.sort_indicator = false;
			arrival_time.sort_order = Gtk.SortType.DESCENDING;
			departure_date.sort_indicator = false;
			departure_date.sort_order = Gtk.SortType.DESCENDING;
			departure_time.sort_indicator = false;
			departure_time.sort_order = Gtk.SortType.DESCENDING;
		}

		/**
		 * Called when a new or old place is saved
		 * @param place_editor The editor that saved the place
		 */
		public void on_save_place(PlaceEditor place_editor) {
			// Not implemented due to lack of time
			/*edit_place.sensitive = false;
			delete_place.sensitive = false;*/
			reset_columns ();

			if (tour.id_tour != 0) {
				list_store.clear ();
				update_list_store ();
			}
		}

		/**
		 * Called when a new or old place is saved
		 * @param place_editor The editor that saved the place
		 */
		public void on_save_tour_place(TourPlaceEditor place_editor, Asociado asociado) {
			// Not implemented due to lack of time
			/*edit_place.sensitive = false;
			delete_place.sensitive = false;*/
			reset_columns ();

			if (tour.id_tour == 0) {
				asociado_list.append (asociado);
				Gtk.TreeIter iter;
				list_store.append (out iter);
				list_store.set (iter,
					Column.PLACE_NAME, asociado.lugar.nombre_lugar,
					Column.TICKET_PRICE, Money.format_uint (asociado.lugar.valor_entrada),
					Column.DIFFICULTY, asociado.lugar.nivel,
					Column.ARRIVAL_DATE, asociado.fecha_llegada,
					Column.ARRIVAL_TIME, asociado.hora_llegada,
					Column.DEPARTURE_DATE, asociado.fecha_salida,
					Column.DEPARTURE_TIME, asociado.hora_salida,
					Column.ASSOCIATED, asociado);
			}
			else {
				list_store.clear ();
				update_list_store ();
			}
		}

		/**
		 * Update the list store with the data from the database
		 */
		private void update_list_store () {
			try {
				associated_list = Asociado.get_all_asociados_by_tour (conn, tour);
			}
			catch (Error e) {
				#if DEBUG
					error (e.message);
				#else
					warning (e.message);
				#endif
			}

			associated_list.foreach ((entry) => {
				Gtk.TreeIter iter;
				list_store.append (out iter);
				list_store.set (iter,
					Column.PLACE_NAME, entry.lugar.nombre_lugar,
					Column.TICKET_PRICE, Money.format_uint (entry.lugar.valor_entrada),
					Column.DIFFICULTY, entry.lugar.nivel,
					Column.ARRIVAL_DATE, entry.fecha_llegada,
					Column.ARRIVAL_TIME, entry.hora_llegada,
					Column.DEPARTURE_DATE, entry.fecha_salida,
					Column.DEPARTURE_TIME, entry.hora_salida,
					Column.ASSOCIATED, entry);
			});
		}

		/**
		 * Initialize the tour list class
		 * @param application The application used to make the GLib object
		 * @param conn The database connection to use
		 * @param tour The tour which is the parent of this window
		 */
		public TourPlaces (Gtk.Application application, Connection conn, Tour tour) {
			Object (application: application);
			this.conn = conn;
			this.tour = tour;

			this.set_visible (true); // This fixes: Gtk-CRITICAL **: 23:58:22.139: gtk_box_gadget_distribute: assertion 'size >= 0' failed in GtkScrollbar
		}

		/**
		 * Initialize what is needed for this window
		 */
		public void initialize () {
			list_store = new Gtk.ListStore (Column.N_COLUMNS,
				typeof (string),
				typeof (string),
				typeof (uint),
				typeof (string),
				typeof (string),
				typeof (string),
				typeof (string),
				typeof (Asociado));

			if (tour.id_tour != 0) {
				update_list_store ();
			}

			place_tree.set_model (list_store);
		}
	}
}
