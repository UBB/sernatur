/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace Sernatur {
	using LibSernatur.DB;

	/**
	 * The tourist add illness window class
	 */
	[GtkTemplate (ui = "/cl/cromer/ubb/sernatur/tourist.illness.editor.ui")]
	public class TouristIllnessEditor : Gtk.ApplicationWindow {
		/**
		 * The open database connection
		 */
		private Connection conn;
		/**
		 * The place data stored in the list store
		 */
		private enum Column {
			/**
			 * The illness description
			 */
			ILLNESS_DESCRIPTION,
			/**
			 * The illness object
			 */
			ILLNESS,
			/**
			 * The number of colums in this enum
			 */
			N_COLUMNS
		}
		/**
		 * The add illness button
		 */
		[GtkChild]
		private Gtk.Button add_illness;
		/**
		 * The close button
		 */
		[GtkChild]
		private Gtk.Button close_illness;
		/**
		 * The illness
		 */
		[GtkChild]
		private Gtk.ComboBoxText illness;
		/**
		 * A list of the illnesses from the database
		 */
		private List<Enfermedad> illnesses;
		/**
		 * The list that stores the illnesses for the combo box
		 */
		private Gtk.ListStore illness_list_store;
		/**
		 * This signal is called when an illness is saved
		 */
		public signal void save_illness ();
		/**
		 * The illness to be added
		 */
		private TieneEnfermedad tiene_enfermedad;

		/**
		 * This callback is called when a button is clicked
		 * @param button The button that was clicked
		 */
		[GtkCallback]
		public void on_clicked_button (Gtk.Button button) {
			if (button == add_illness) {
				// Insert is all that is needed
				update_illness_instance ();
				try {
					TieneEnfermedad.insert_illness (conn, tiene_enfermedad);
				}
				catch (Error e) {
					#if DEBUG
						error (e.message);
					#else
						warning (e.message);
					#endif
				}
				save_illness (); // Signal the parent to update itself
				this.close ();
			}
			else if (button == close_illness) {
				this.close ();
			}
		}

		/**
		 * Update the the illness object with new info from the editor
		 */
		private void update_illness_instance () {
			Enfermedad enfermedad;
			Gtk.TreeIter iter;
			illness.get_active_iter (out iter);
			illness_list_store.get (iter,
				Column.ILLNESS, out enfermedad);
			tiene_enfermedad.enfermedad = enfermedad;
		}

		/**
		 * Initialize the tour illness editor class
		 * @param application The application used to make the GLib object
		 * @param conn The database connection to use
		 * @param tiene_enfermedad The place to edit
		 */
		public TouristIllnessEditor (Gtk.Application application, Connection conn, TieneEnfermedad tiene_enfermedad) {
			Object (application: application);
			this.conn = conn;
			this.tiene_enfermedad = tiene_enfermedad;
		}

		/**
		 * Initialize what is needed for this window
		 */
		public void initialize () {
			try {
				illnesses = Enfermedad.get_all_enfermedades (conn);
			}
			catch (Error e) {
				#if DEBUG
					error (e.message);
				#else
					warning (e.message);
				#endif
			}

			illnesses.sort_with_data ((a, b) => {
				return strcmp (a.descripcion_enfermedad, b.descripcion_enfermedad);
			});

			illness_list_store = new Gtk.ListStore (Column.N_COLUMNS,
				typeof (string),
				typeof (Enfermedad));

			illness.set_model (illness_list_store);

			try {
				var has_illness_list = TieneEnfermedad.get_all_tiene_enfermedades_by_run (conn, tiene_enfermedad.turista.rut_turista);
				illnesses.foreach ((entry) => {
					var can_add = true;
					has_illness_list.foreach ((entry2) => {
						if (entry.id_enfermedad == entry2.enfermedad.id_enfermedad) {
							can_add = false;
						}
					});
					if (can_add) {
						Gtk.TreeIter iter;
						illness_list_store.append (out iter);
						illness_list_store.set (iter,
							Column.ILLNESS_DESCRIPTION, entry.descripcion_enfermedad,
							Column.ILLNESS, entry);
					}
				});
				illness.set_active (0);
			}
			catch (Error e) {
				#if DEBUG
					error (e.message);
				#else
					warning (e.message);
				#endif
			}
		}
	}
}
