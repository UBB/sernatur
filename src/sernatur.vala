/*
 * Copyright 2018-2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * The sernatur program
 */
namespace Sernatur {
	using Constants;
	/**
	 * If version is passed as an option on the console line
	 */
	private static bool version = false;
	/**
	 * The valid options that can be used on the console line
	 */
	private const GLib.OptionEntry[] options = {
		{ "version", 'v', 0, OptionArg.NONE, ref version, "Display version number", null },
		{ null }
	};

	/**
	 * The application class
	 */
	public class Application : global::Gtk.Application {
		/**
		 * Initialize the application
		 */
		public Application () {
			application_id = "cl.cromer.ubb.sernatur";
		}

		/**
		 * Run when the application is activated
		 */
		public override void activate () {
			var window = new MainWindow (this);
			var settings = new GLib.Settings ("cl.cromer.ubb.sernatur.window");

			window.set_default_size (settings.get_int ("width"), settings.get_int ("height"));

			if (settings.get_boolean ("maximized")) {
				window.maximize ();
			}

			window.icon = new Gtk.Image.from_resource ("/cl/cromer/ubb/sernatur/pixdata/icon-sernatur.png").get_pixbuf ();
			window.show_all ();
			window.initialize ();
		}

		/**
		 * Run when the application starts
		 */
		public override void startup () {
			base.startup ();
		}
	}

	/**
	 * The main function
	 * @param args Arguments passed from the command line
	 * @return success of the application
	 */
	public static int main (string[] args) {
		Intl.setlocale (LocaleCategory.ALL, "");
		Intl.bindtextdomain (GETTEXT_PACKAGE, "/usr/share/locale");
		Intl.bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
		Intl.textdomain (GETTEXT_PACKAGE);
		try {
			var opt_context = new OptionContext ("");
			opt_context.set_translation_domain (GETTEXT_PACKAGE);
			opt_context.set_help_enabled (true);
			opt_context.add_main_entries (options, null);
			opt_context.parse (ref args);
		}
		catch (OptionError e) {
			print (_ ("Error: %s\n"), e.message);
			print (_ ("Run '%s --help' to see a full list of available command line options.\n"), args[0]);
			return 1;
		}

		if (version) {
			print (_ ("SERNATUR version: ") + VERSION + "\n");
			return 0;
		}
		else {
			var application = new Application ();
			return application.run (args);
		}
	}
}
